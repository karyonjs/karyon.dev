import { State, Context, evolve, message, multicast } from '/karyon.js';

const now = () => new Date(Date.now() - new Date().getTimezoneOffset() * 60000)
    .toISOString().replace(/[TZ]/g, ' ').trim();

const emit = message => {
    const {event, log} = message.data ?? {};
    if ('dir log info warn error'.includes(event))
        multicast('log', event, log);
};

const Editor = (() => {

    const options = {
        tabSize: 4, indentUnit: 4,
        foldGutter: true, lineNumbers: true, lineWrapping: true,
        gutters: ['CodeMirror-foldgutter', 'CodeMirror-linenumbers'],
        extraKeys: {'Ctrl-F' (CM) { CM.foldCode(CM.getCursor()); }}
    };

    const mime = {
        js: {mode: 'javascript', type: 'application/javascript'},
        css: {mode: 'css', type: 'text/css'},
        html: {mode: 'htmlmixed', type: 'text/html'},
        txt: {mode: 'txt', type: 'text/plain'}
    };

    const create = (name, text) => {
        const ext = name.slice(name.lastIndexOf('.')).slice(1).toLowerCase();
        const {mode, type} = mime[ext] ?? mime.txt;
        const Doc = CodeMirror.Doc(text, mode);
        return {name, ext, type, text, Doc};
    };

    const open = function (fd, text) {
        const init = arguments.length === 2;
        const name = fd.name ?? fd;
        const file = init ? create(name, text) : this.files()
            .find(file => file.name === name);
        init && this.files([...this.files(), file]);
        return this.file(file);
    };

    const reset = function () {
        this.files().forEach(file => file.Doc.setValue(file.text));
    };

    const swap = (context, file) => {
        context?.CM?.swapDoc(file?.Doc ?? CodeMirror.Doc(''));
    };

    const observe = {
        mount () {
            this.CM = CodeMirror(this.target, options);
            swap(this, this.file());
        },
        unmount () { swap(this); },
        resize () { this.CM.refresh(); }
    };

    return () => {
        const files = State([]);
        const file = State();
        const editor = {class: 'editor', files, file, open, reset, observe};
        State.on(file, file => swap(Context(editor), file));
        return editor;
    };

})();

const Simulator = ({origin, path, editor}) => {

    let timeout;
    const src = `${origin}/${path}/`;
    const wait = State(true);

    const ready = set => {
        if (set) {
            clearTimeout(timeout);
            wait(false);
            return;
        }
        if (wait())
            return;
        wait(true);
        timeout = setTimeout(ready, 3000, true);
        return true;
    };

    const submit = ({target}, message, ports) => {
        if (ports || ready())
            target.contentWindow.postMessage(message, target.src, ports);
    };

    const listen = {
        load () {
            const {target} = this;
            const init = setTimeout(() => {
                target.src = `${origin}/#init`;
            }, 1000);
            const wire = new MessageChannel();
            wire.port1.onmessage = ({data}) => {
                if (data?.event !== 'ready')
                    return;
                clearTimeout(init);
                if (target.src !== src)
                    return void (target.src = src);
                ready(true);
                const wire = new MessageChannel();
                wire.port1.onmessage = emit;
                submit(this, {event: 'ready'}, [wire.port2]);
            };
            submit(this, {event: 'init'}, [wire.port2]);
        }
    };

    const actions = {
        start () {
            const files = editor.files().map(({name, type, Doc}) => ({
                path: `/${path}/${name}`, type, content: Doc.getValue()
            }));
            submit(this, {event: 'start', data: {src, files}});
        },
        reload () { submit(this, {event: 'reload', data: {src}}); },
        reset () { submit(this, {event: 'reset', data: {src}}); },
    };

    return {class: 'simulator', is: 'iframe',
        attrs: {src, 'data-wait': wait},
        listen, observe: {message (id) { actions[id]?.call(this); }}
    };
};

const Ruler = (() => {
    
    const digest = size => size > 96 ? 100 : size < 6 ? 0 :
        Math.min(Math.max(size, 30), 80);

    const snap = (to => size => to[size])({0: 'min', 100: 'max'});

    const listen = {
        dblclick () { this.size(this.init); },
        pointerdown (event) {
            event.target.setPointerCapture(event.pointerId);
        },
        pointermove (event) {
            if (!event.target.hasPointerCapture(event.pointerId))
                return;
            const size = this.target.parentNode.getBoundingClientRect();
            this.size(this.axis === 'x'
                ? (event.clientX - size.left) / size.width * 100
                : (event.clientY - size.top) / size.height * 100);
        }
    };

    return (axis, init) => {
        const size = State(init, {digest});
        return {class: `ruler ${axis}`, attrs: {'data-snap': snap}, listen,
            axis, init, size, value: () => size().toFixed(2), data: size
        };
    };

})();

const IDE = ({origin, path, ruler = 55}, options) => {

    const alarm = State();
    const [rulerX, rulerY] = [Ruler('x', ruler), Ruler('y', ruler)];
    const editor = Editor();
    const simulator = Simulator({origin, path, editor});

    const start = () => message(simulator, 'start');
    const reset = () => (message(simulator, 'reset'), editor.reset());

    const editorMenu = {class: 'menu', content: [
        {class: 'files', content: () => editor.files().map(file => ({
            class: 'action file code',
            attrs: {
                'data-name': file.name, 'data-ext': file.ext,
                'data-active': () => editor.file() === file
            },
            listen: {click () { editor.open(file.name); }}
        }))},
        {class: 'actions', content: [
            {class: 'action start', listen: {click: start}},
            {class: 'action reset', listen: {click: reset}}
        ]}
    ]};

    const simulatorMenu = {class: 'menu', content: [
        {class: 'title', content: 'simulator'},
        {class: 'actions', content: [
            {class: 'action console',
                attrs: {
                    'data-on': () => logger.state(),
                    'data-error': alarm
                },
                listen: {click () { multicast('logger'); }}},
            {class: 'action spawn',
                listen: {click () {
                    const src = Context(simulator)?.target.src;
                    if (src === `${origin}/${path}/`)
                        window.open(src, `KARYON/Simulator/${path}/`);
                }}},
            {class: 'action reload',
                listen: {click () { message(simulator, 'reload'); }}}
        ]}
    ]};

    const log = State([], {digest: ([event, content], log) => {
        if (!event)
            return [];
        const row = {class: 'log', event, content,
            attrs: {'data-level': event, title: now(), tabindex: 0}};
        return [row, ...log].slice(0, options?.logger?.size ?? 300);
    }});

    const loggerMenu = {class: 'menu', content: [
        {class: 'title', content: 'console'},
        {class: 'actions', content: [
            {class: 'action time', attrs: {'data-on': () => logger.timing()},
                listen: {click () { logger.timing(!logger.timing()); }}},
            {class: 'action clear',
                listen: {click () { multicast('log'); }}},
            {class: 'action close',
                listen: {click () { multicast('logger', false); }}}
        ]}
    ]};

    const logger = {class: 'logger', state: State(), timing: State(),
        attrs: {'data-timing' () { return this.timing(); }, tabindex: -1},
        content: [loggerMenu, {class: 'output', content: log}],
        keymap: {keys: 'Esc', action () { this.state(null); }},
        observe: {attach () { this.target.focus(); }}
    };

    const channels = {
        log (...args) { log(args); },
        logger (on) { logger.state(on ?? !logger.state()); }
    };

    evolve(editor, {keymap: [
        {keys: 'Ctrl S', action: start, prevent: 5, options: {capture: true}},
        {keys: 'Ctrl R', action: reset, prevent: 5, options: {capture: true}},
        {keys: ['Ctrl S', 'Ctrl R'], weak: true,
            prevent: 1, options: {capture: true}}
    ]});

    options?.logger?.active && logger.state(true);
    options?.logger?.timing && logger.timing(true);
    State.on(log, ([row]) => row?.event === 'error' && alarm(!logger.state()));
    State.on(logger.state, on => on && alarm(null));

    return {class: 'ide', editor, path,
        observe: {multicast: {channels: ['log', 'logger'],
            action (channel, ...args) { channels[channel]?.(...args); }
        }},
        content: [
            {class: 'editor-view', content: [editorMenu, editor]},
            rulerX, rulerY,
            {class: 'simulator-view', content: [
                simulatorMenu, simulator, logger
            ]}
        ],
        style: {'--ruler-x': rulerX.value, '--ruler-y': rulerY.value}
    };

};

export default IDE;
