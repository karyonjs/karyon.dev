import { State, Component, insert } from '/karyon.js';
import './api.js';
import './components.js';
import IDE from './ide.js';
import meta from '/samples/meta.js';

const origin = 'https://simulator.karyon.dev';
const paths = new Map();
const ide = State();

const envs = meta.map(({name, type, samples}) => ({
    name, type, samples: samples.map(({name, path, files, logger}) => {
        const ide = IDE({origin, path}, {logger});
        paths.set(`#${path}`, ide);
        return Object.assign(ide, {name, files});
    })}
));

const contents = Component('page-contents', {
    content: {is: 'ul', content: [
        {is: 'li', content: envs.map(meta => [
            {is: 'h3', content: meta.name},
            {is: 'ul', content: meta.samples.map(IDE => ({
                is: 'li', content: {
                    is: 'a', content: IDE.name,
                    attrs: {
                        href: `#${IDE.path}`,
                        'data-active': () => ide() === IDE
                    }
                }
            }))
        }])}
    ]}
});

const load = () => {
    const IDE = paths.get(location.hash);
    !IDE && (location.hash = `#${envs[0].samples[0].path}`);
    if (!IDE || ide(IDE).ready)
        return;
    IDE.ready = true;
    IDE.files.map(fd => {
        const file = IDE.editor.open(fd, '');
        fetch(fd.url ?? `/samples/${IDE.path}/${fd}`).then(res => res.text()
            .then(text => file.Doc.setValue(file.text = text)));
    });
    IDE.editor.open(IDE.files[0]);
};

addEventListener('hashchange', load);

load();

insert([
    Component('page-nav', {id: 'workbench', contents}),
    {is: 'main', id: 'workbench', content: [contents, ide]}
], document.body);
