const ID = 'API';
const TITLE = 'API';
const MDN = `//developer.mozilla.org/en-US/docs`;
const MDNWeb = `${MDN}/Web`;
const MDNRef = `${MDNWeb}/JavaScript/Reference/Global_Objects`;
const MDNAPI = `${MDNWeb}/API`;
const LF = Symbol();

const callables = new Set(['function', 'method', 'callback']);
const docs = new Map([
    ['JavaScript', `${MDNWeb}/JavaScript`],
    ['CSS', `${MDNWeb}/CSS`],
    ['HTML', `${MDNWeb}/HTML/Element`],
    ['SVG', `${MDNWeb}/SVG/Element`],
    ['MathML', `${MDNWeb}/MathML/Element`],
    ['KARYON', `//gitlab.com/karyonjs/karyon`],
    ['void', `//en.wikipedia.org/wiki/Empty_type`],
]);
const directives = {
    ns: '#model',
    is: '#model',
    id: '#model/properties',
    props: '#model/properties',
    attrs: '#model/attributes',
    class: '#model/attributes',
    style: '#model/attributes',
    content: '#model/content',
    shadow: '#model/shadow',
    observe: '#model/signals',
    listen: '#model/events',
    keymap: '#extras/keyboard-shortcuts',
    animate: '#extras/animations',
    mixins: '#composition/mixins',
    options: './#API/model.options',
    state: '#model/state',
    init: './#API/model.init()'
};
const types = new Map([
    ['null', `${MDNRef}/null`],
    ['undefined', `${MDNRef}/undefined`],
    ['primitive', `${MDN}/Glossary/Primitive`],
    ['boolean', `${MDN}/Glossary/Boolean`],
    ['truthy', `${MDN}/Glossary/Truthy`],
    ['falsy', `${MDN}/Glossary/Falsy`],
    ['symbol', `${MDN}/Glossary/Symbol`],
    ['any', `${MDNWeb}/JavaScript/Data_structures`],
    ['string', `${MDNWeb}/JavaScript/Data_structures#string_type`],
    ['number', `${MDNWeb}/JavaScript/Data_structures#number_type`],
    ['integer', `${MDNWeb}/JavaScript/Data_structures#number_type`],
    ['hexadecimal', `${MDNWeb}/JavaScript/Guide/Numbers_and_dates#hexadecimal_numbers`],
    ['function', `${MDNRef}/Function`],
    ['this', `${MDNWeb}/JavaScript/Reference/Operators/this`],
    ['arguments', `${MDNWeb}/JavaScript/Reference/Functions/arguments`],
    ['class', `${MDNWeb}/JavaScript/Reference/Classes`],
    ['Array', `${MDNRef}/Array`],
    ['Object', `${MDNRef}/Object`],
    ['toString()', `${MDNRef}/Object/toString`],
    ['Object.assign()', `${MDNRef}/Object/assign`],
    ['Promise', `${MDNRef}/Promise`],
    ['Proxy', `${MDNRef}/Proxy`],
    ['Error', `${MDNRef}/Error`],
    ['TypeError', `${MDNRef}/TypeError`],
    ['ReferenceError', `${MDNRef}/ReferenceError`],
    ['call()', `${MDNRef}/Function/call`],
    ['window', `${MDNAPI}/Window`],
    ['input', `${MDNWeb}/HTML/Element/input`],
    ['select', `${MDNWeb}/HTML/Element/select`],
    ['textarea', `${MDNWeb}/HTML/Element/textarea`],
    ['template', `${MDNWeb}/HTML/Element/template`],
    ['Node', `${MDNAPI}/Node`],
    ['childNodes', `${MDNAPI}/Node/childNodes`],
    ['HTML.attributes', `${MDNWeb}/HTML/Global_attributes/class`],
    ['SVG.svg', `${MDNWeb}/SVG/Element/svg`],
    ['SVG.foreignObject', `${MDNWeb}/SVG/Element/foreignObject`],
    ['Element', `${MDNAPI}/Element`],
    ['Element.id', `${MDNAPI}/Element/id`],
    ['Element.attributes', `${MDNAPI}/Element/attributes`],
    ['Element.properties', `${MDNAPI}/Element#properties`],
    ['Element.namespaceURI', `${MDNAPI}/Element/namespaceURI`],
    ['Element.class', `${MDNWeb}/HTML/Global_attributes/class`],
    ['Element.style', `${MDNWeb}/HTML/Global_attributes/style`],
    ['Element.contenteditable', `${MDNWeb}/HTML/Global_attributes/contenteditable`],
    ['Element.tabindex', `${MDNWeb}/HTML/Global_attributes/tabindex`],
    ['Element.data-*', `${MDNWeb}/HTML/Global_attributes/data-*`],
    ['Element.aria-*', `${MDNWeb}/Accessibility/ARIA`],
    ['Element.attachShadow()', `${MDNAPI}/Element/attachShadow`],
    ['Element.setHTML()', `${MDNAPI}/Element/setHTML`],
    ['CustomElementRegistry.define()', `${MDNAPI}/CustomElementRegistry/define`],
    ['ShadowRoot', `${MDNAPI}/ShadowRoot`],
    ['ShadowRoot.adoptedStyleSheets', `${MDNAPI}/ShadowRoot/adoptedStyleSheets`],
    ['nodeName', `${MDNAPI}/Node/nodeName`],
    ['tagName', `${MDNAPI}/Element/tagName`],
    ['innerHTML', `${MDNAPI}/Element/innerHTML`],
    ['textContent', `${MDNAPI}/Node/textContent`],
    ['Text', `${MDNAPI}/Text`],
    ['Comment', `${MDNAPI}/Comment`],
    ['CDATASection', `${MDNAPI}/CDATASection`],
    ['CharacterData', `${MDNAPI}/CharacterData`],
    ['Event', `${MDNAPI}/Event`],
    ['CustomEvent', `${MDNAPI}/CustomEvent`],
    ['Event.preventDefault()', `${MDNAPI}/Event/preventDefault`],
    ['Event.stopPropagation()', `${MDNAPI}/Event/stopPropagation`],
    ['Event.stopImmediatePropagation()', `${MDNAPI}/Event/stopImmediatePropagation`],
    ['Event.defaultPrevented', `${MDNAPI}/Event/defaultPrevented`],
    ['EventTarget', `${MDNAPI}/EventTarget`],
    ['EventTarget.addEventListener()::options', `${MDNAPI}/EventTarget/addEventListener#options`],
    ['MutationObserver', `${MDNAPI}/MutationObserver`],
    ['ResizeObserver', `${MDNAPI}/ResizeObserver`],
    ['IntersectionObserver', `${MDNAPI}/IntersectionObserver`],
    ['CSSStyleSheet', `${MDNAPI}/CSSStyleSheet`],
    ['Keyframe', `${MDNAPI}/Web_Animations_API/Keyframe_Formats`],
    ['Animation.ready', `${MDNAPI}/Animation/ready`],
    ['Animation.persist()', `${MDNAPI}/Animation/persist`],
    ['AnimationPlaybackEvent', `${MDNAPI}/AnimationPlaybackEvent`],
    ['Element.animate()::options', `${MDNAPI}/Element/animate#options`],
    ['queueMicrotask', `${MDNAPI}/queueMicrotask`],
    ['GC', `${MDNWeb}/JavaScript/Memory_Management#garbage_collection`]
]);

export const heading = content => ({is: 'h3', content});

const link = (href, content, clist) => ({
    is: 'a', class: clist, content,
    attrs: href[0] === '#' ? {href} : {href, target: '_blank', rel: 'noopener'}
});

const plink = href => ({is: 'a', attrs: {href}, class: 'permalink'});

const typeRef = type => {
    const href = types.get(type);
    return href ? link(href, type) : type;
};

const typeDef = type => typeof type === 'string'
    ? {is: 'span', content: typeRef(type)}
    : [
        {is: 'span', content: typeRef(type.type)},
        '❮', Array.isArray(type.of) ? type.type === 'Array'
        ? typeDoc(type.of) : [
            {is: 'span', content: typeRef(type.of[0])}, ', ',
            typeDoc(type.of[1])
        ] : typeDef(type.of), '❯'
    ];

const typeDoc = type => [type].flat().map((type, i, arr) => type && [
    i ? [arr[i - 1] === LF ? '\n' :
        {is: 'span', class: 'pipe', content: '❙'}] : null,
    type === LF ? null : typeDef(type)
]);

const codeDef = value => ({is: 'dl', content: [value].flat().map(param => [
    {is: 'dt', content: [
        param.name && {is: 'code', class: 'typedef name', content: param.name},
        {is: 'code', class: 'typedef', content: typeDoc(param.type)},
        param.attributes?.map(name => ({is: 'code', content: name})),
        'default' in param && {is: 'code', class: 'default', content: [
            'DEFAULT: ', {is: 'span', class: 'value', content:
                typeof param.default === 'string'
                ? param.default : JSON.stringify(param.default)}
        ]}
    ]},
    param.details && {is: 'dd', content: param.details}
])});

const def = (name, value) => value && [heading(name), codeDef(value)];

const tokenize = (slice, token) => {
    if ((token ?? '') === '')
        return [slice];
    if (token === '/')
        return [slice, {is: 'br'}];
    if (typeof token === 'function')
        return [slice, token];
    if (typeof token === 'object')
        return [slice, codeDef(token)];
    if (typeof token !== 'string')
        return [slice, {is: 'code', content: typeRef(token)}];
    const refdoc = docs.get(token);
    if (refdoc) {
        const lnk = link(refdoc, token, 'ref');
        return [slice, token === 'KARYON'
            ? {is: 'code', class: 'karyon', content: lnk} : lnk];
    }
    const content = token.slice(2);
    if (token.startsWith('@:'))
        return [slice, {is: 'code', attrs: {'data-kind': content}}];
    if (token.startsWith('::'))
        return [slice, {
            is: 'a', class: 'directive', content,
            attrs: {href: directives[content]}
        }];
    if (token.startsWith('.:'))
        return [slice, {is: 'code', content}];
    if (token.startsWith('&:'))
        return [slice, {is: 'code', class: 'key', content}];
    if (token.startsWith('=:'))
        return [slice, {is: 'code', class: 'value', content}];
    if (token.startsWith('!:'))
        return [slice, {is: 'b', class: 'context', content}];
    if (token.startsWith('#:')) {
        const [name, href] = content.split('|');
        const ref = directives[content] ?? types.get(href) ?? href;
        const text = name.startsWith('?:')
            ? tokenize(null, name.slice(2))[1] : name;
        return [slice, link(ref || text, text, 'ref')];
    }
    if (token.match(/^<[^]+>$/)) {
        const href = `${docs.get('HTML')}/${token.slice(1, -1)}`;
        return [slice, link(href, token, 'ref')];
    }
    return [slice, {is: 'code', content: typeRef(token)}];
};

export const $ = (slices, ...tokens) => () => slices.flatMap((slice, i) =>
    tokenize(slice, tokens[i]));

export const text = (...args) => ({is: 'p', content: $(...args)});

export const code = (text, options) => {
    const {lang, indent: s = 8} = options ?? {};
    text = text.replace(new RegExp(`(\\n\\s{${s}})`, 'g'), '\n')
        .replace(/^\n|\s+$/g, '');
    if (lang === 'sh')
        return {class: 'sample', content:
            {is: 'code', class: 'typedef name sh', content: text}
        };
    return {class: 'sample', style: {'--lines': text.split('\n').length},
        observe: {attach () {
            const idle = window.requestIdleCallback;
            (idle ?? setTimeout)(() => {
                const CM = CodeMirror(this.target, {readOnly: true});
                CM.swapDoc(CodeMirror.Doc(text, lang ?? 'javascript'));
            }, idle ? {timeout: 1000} : 1000);
        }}
    };
};

export const list = (...values) => ({is: 'ul', class: 'pre', content:
    values.map(content => ({is: 'li', content}))});

export const section = (uri => (title, content) => {
    const id = uri(title);
    return [
        {is: 'h1', id, content: [plink(`#${id}`), title]},
        ...content.map(({title, content}, i) => {
            const href = `${id}/${uri(title)}`;
            return i === 0
                ? [{is: 'article', content}]
                : {is: 'article', content: [
                    {is: 'h2', id: href, content: [plink(`#${href}`), title]},
                    content
                ]}
        })
    ];
})(title => (title ?? '').replace(/ /g, '-').toLowerCase());

const API = [{
    kind: 'typedef',
    name: 'model',
    type: 'Object',
    properties: [{
        name: 'ns',
        type: 'string',
        default: 'html',
        attributes: ['OPTIONAL'],
        details: [
            $`The ${`${'::ns'}`} directive, accepts a namespace ${'=:alias'} or
                ${`${'#:URI'}|${MDNAPI}/Document/createElementNS#important_namespace_uris`},
                as follows:`,
            text`${'=:html'} or ${'#:http://www.w3.org/1999/xhtml'}`,
            text`${'=:svg'} or ${'#:http://www.w3.org/2000/svg'}`,
            text`${'=:math'} or ${'#:http://www.w3.org/1998/Math/MathML'}`,
            text`If not defined, inherits the namespace of parent model,
                or ${'=:html'} otherwise.`
        ]
    }, {
        name: 'is',
        type: 'string',
        default: 'div',
        attributes: ['OPTIONAL'],
        details: [
            $`The ${`${'::is'}`} directive, defined as follows:`,
            text`${'tagName'} of ${'HTML'}, ${'SVG'} or ${'MathML'} element.`,
            text`${'nodeName'} of ${'CharacterData'} node, one of:`,
            text`${'=:#text'} ${'=:#comment'} ${'=:#cdata-section'}`
        ]
    }, {
        name: 'id',
        type: 'string',
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::id'}`} directive definition.`
    }, {
        name: 'props',
        type: 'model::defs',
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::props'}`} directive definition.`
    }, {
        name: 'attrs',
        type: 'model::defs',
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::attrs'}`} directive definition.`
    }, {
        name: 'class',
        type: ['model::defs', 'state()', 'model::resolver()', 'any'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::class'}`} directive definition.`
    }, {
        name: 'style',
        type: ['model::defs', 'state()', 'model::resolver()', 'any'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::style'}`} directive definition.`
    }, {
        name: 'content',
        type: ['state()', 'model::resolver()', 'any'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::content'}`} directive definition.`
    }, {
        name: 'shadow',
        type: ['state()', 'model::resolver()', 'any'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::shadow'}`} directive definition.`
    }, {
        name: 'observe',
        type: ['model::observe', {type: 'Array', of: 'model::observe'}],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::observe'}`} directive definition.`
    }, {
        name: 'listen',
        type: ['model::listen', {type: 'Array', of: 'model::listen'}],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::listen'}`} directive definition.`
    }, {
        name: 'keymap',
        type: ['model::keymap', {type: 'Array', of: 'model::keymap'}],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::keymap'}`} directive definition.`
    }, {
        name: 'animate',
        type: ['model::animate', {type: 'Array', of: 'model::animate'}],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::animate'}`} directive definition.`
    }, {
        name: 'mixins',
        type: {type: 'Array', of: 'model::mixin'},
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::mixins'}`} directive definition.`
    }, {
        name: 'options',
        type: 'model.options',
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::options'}`} directive definition.`
    }, {
        name: 'state',
        type: ['state()', 'model::resolver()'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::state'}`} directive definition.`
    }, {
        name: 'init',
        type: 'model.init()',
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::init'}`} directive definition.`
    }],
    details: $`The ${'#:model|#model'} definition.`
}, {
    kind: 'typedef',
    name: 'model::mixin',
    type: 'Object',
    properties: [{
        name: 'props',
        type: 'model::defs',
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::props'}`} directive definition.`
    }, {
        name: 'attrs',
        type: 'model::defs',
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::attrs'}`} directive definition.`
    }, {
        name: 'class',
        type: ['model::defs', 'state()', 'model::resolver()', 'any'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::class'}`} directive definition.`
    }, {
        name: 'style',
        type: ['model::defs', 'state()', 'model::resolver()', 'any'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::style'}`} directive definition.`
    }, {
        name: 'content',
        type: ['state()', 'model::resolver()', 'any'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::content'}`} directive definition.`
    }, {
        name: 'shadow',
        type: ['state()', 'model::resolver()', 'any'],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::shadow'}`} directive definition.`
    }, {
        name: 'observe',
        type: ['model::observe', {type: 'Array', of: 'model::observe'}],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::observe'}`} directive definition.`
    }, {
        name: 'listen',
        type: ['model::listen', {type: 'Array', of: 'model::listen'}],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::listen'}`} directive definition.`
    }, {
        name: 'keymap',
        type: ['model::keymap', {type: 'Array', of: 'model::keymap'}],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::keymap'}`} directive definition.`
    }, {
        name: 'animate',
        type: ['model::animate', {type: 'Array', of: 'model::animate'}],
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::animate'}`} directive definition.`
    }, {
        name: 'options',
        type: 'model.options',
        attributes: ['OPTIONAL'],
        details: $`The ${`${'::options'}`} directive definition.`
    }],
    details: $`The ${'#:mixin|#composition/mixins'} definition.`
}, {
    kind: 'object',
    name: 'model::context',
    type: 'Object',
    properties: [{
        name: 'ns',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::ns'}`} directive value.`
    }, {
        name: 'is',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::is'}`} directive value.`
    }, {
        name: 'id',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::id'}`} directive value.`
    }, {
        name: 'props',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::props'}`} directive value.`
    }, {
        name: 'attrs',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::attrs'}`} directive value.`
    }, {
        name: 'class',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::class'}`} directive value.`
    }, {
        name: 'style',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::style'}`} directive value.`
    }, {
        name: 'content',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::content'}`} directive value.`
    }, {
        name: 'shadow',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::shadow'}`} directive value.`
    }, {
        name: 'observe',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::observe'}`} directive value.`
    }, {
        name: 'listen',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::listen'}`} directive value.`
    }, {
        name: 'keymap',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::keymap'}`} directive value.`
    }, {
        name: 'animate',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::animate'}`} directive value.`
    }, {
        name: 'mixins',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::mixins'}`} directive value.`
    }, {
        name: 'options',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::options'}`} directive value.`
    }, {
        name: 'state',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::state'}`} directive value.`
    }, {
        name: 'init',
        type: ['any'],
        attributes: ['READONLY'],
        details: $`The ${`${'::init'}`} directive value.`
    }, {
        name: 'model',
        type: ['model', 'undefined'],
        attributes: ['READONLY'],
        details: 'Context owner.'
    }, {
        name: 'target',
        type: ['Node', 'null'],
        attributes: ['READONLY'],
        details: 'Model target.'
    }, {
        name: 'parent',
        type: ['model', 'undefined'],
        attributes: ['READONLY'],
        details: 'Parent model.'
    }, {
        name: 'root',
        type: ['model', 'undefined'],
        attributes: ['READONLY'],
        details: 'Closest component ancestor.'
    }, {
        name: 'data',
        type: ['any'],
        attributes: ['OPTIONAL'],
        details: $`Value bound as ${'data'} argument of ${'model::resolver()'},
            see ${'#:Resolvers|#reactivity/resolvers'} for details.`
    }],
    details: $`The runtime context of ${'model'}.`
}, {
    kind: 'typedef',
    name: 'model::defs',
    type: {type: 'Object', of: [
        'string', ['state()', 'model::resolver()', 'any']
    ]},
    details: [
        text`A ${'❮key,value❯'} dictionary of directive definitions,
        an example of ${'::style'} directive definition is below:`,
        code(`let box = {style: {display: 'flex', opacity: 1}};`)
    ]
}, {
    kind: 'callback',
    name: 'model::resolver()',
    context: {
        type: 'model::context'
    },
    params: [{
        name: 'data',
        type: 'any',
        details: $`Computed value of ${'model::context'} ${'data'} property.`
    }],
    returns: {
        type: 'any'
    },
    details: $`Provides a computed value for a certain DOM operation.`
}, {
    kind: 'typedef',
    name: 'model::observe',
    type: [
        {type: 'Object', of: ['string', [
            LF,
            'model::observe::handler',
            LF,
            'model::observe::handler.action()',
            LF,
            {type: 'Array', of: [
                'model::observe::handler', 'model::observe::handler.action()'
            ]}
        ]]}
    ],
    details: $`A dictionary of signal handlers. The keys are signals verbs such
        as ${'&:mount'} or ${'&:error'}.`
}, {
    kind: 'typedef',
    name: 'model::observe::handler',
    type: 'Object',
    properties: [{
        name: 'action',
        type: 'model::observe::handler.action()',
        attributes: ['REQUIRED'],
        details: $`Signal handler callback.`
    }, {
        name: 'channels',
        type: [{type: 'Array', of: 'any'}, 'any'],
        attributes: ['OPTIONAL'],
        details: $`Channels identifiers for ${'&:multicast'} signals.`
    }, {
        name: 'options',
        type: 'model::observe::handler.options',
        attributes: ['OPTIONAL'],
        details: $`The initialization options of the observers-driven signals.`
    }],
    details: $`${'#:Signals|#model/signals'} handler object. Here are defined
        the properties specific for this object only, for an extended handler
        configuration the ${'emitter.on()::handler'} object should be consulted.`
}, {
    kind: 'callback',
    name: 'model::observe::handler.action()',
    context: {
        type: 'model::context'
    },
    params: [{
        name: '...args',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'Callback arguments, varying length.'
    }, {
        name: '...payload',
        type: 'any',
        attributes: ['REQUIRED'],
        details: [
            $`Payload arguments, varying length, signal dependant
            as follows:`,
            text`${'&:mount'}: [${'Node'}]`,
            text`${'&:unmount'}: [${'Node'}]`,
            text`${'&:attach'}: [${'Node'}]`,
            text`${'&:detach'}: [${'Node'}]`,
            text`${'&:error'}: [${'Error'}, ${'model::context'}]`,
            text`${'&:message'} ‒ ${'message()'} function payload parameters.`,
            text`${'&:multicast'} ‒ ${'multicast()'} function call parameters.`,
            text`${'&:mutate'} ‒ ${'MutationObserver'}
                ${`${'#:callback parameters'}|${MDNAPI}/MutationObserver/MutationObserver#callback`}.`,
            text`${'&:resize'} ‒ ${'ResizeObserver'}
                ${`${'#:callback parameters'}|${MDNAPI}/ResizeObserver/ResizeObserver#callback`}.`,
            text`${'&:intersect'} ‒ ${'IntersectionObserver'}
                ${`${'#:callback parameters'}|${MDNAPI}/IntersectionObserver/IntersectionObserver#callback`}.`
        ]
    }],
    details: $`${'#:Signals|#model/signals'} handler callback. It MAY act also
        as a handler object when defines the ${'OPTIONAL'} properties of
        ${'model::observe::handler'} object.`
}, {
    kind: 'typedef',
    name: 'model::observe::handler.options',
    type: 'Object',
    properties: [{
        name: 'root',
        type: ['model', 'Element', 'model::observe::viewport()'],
        attributes: ['OPTIONAL'],
        details: $`${'IntersectionObserver'} viewport or viewport
            resolver callback.`
    }],
    details: $`The initialization ${'options'} of ${'IntersectionObserver'},
        ${'MutationObserver'} and ${'ResizeObserver'} observers.${'/'}
        This definition describes only the system enhanced properties of the
        ${'options'} object. For a complete list of properties the
        respective observer specification should be consulted.`
}, {
    kind: 'callback',
    name: 'model::observe::viewport()',
    context: {
        type: 'model::context'
    },
    returns: {
        type: ['model', 'Element']
    },
    details: $`${'IntersectionObserver'} viewport resolver.`
}, {
    kind: 'typedef',
    name: 'model::listen',
    type: [
        {type: 'Object', of: [
            'string', [
                LF,
                'model::listen::handler',
                LF,
                'model::listen::handler.action()',
                LF,
                {type: 'Array', of: [
                    'model::listen::handler', 'model::listen::handler.action()'
                ]}
            ]
        ]}
    ],
    details: $`A dictionary of ${'model'} ${'#:events|#model/events'} handlers.
        The keys are DOM event types, such as generic ${'&:click'} or
        ${'&:pointerdown'} or custom event types.`
}, {
    kind: 'typedef',
    name: 'model::listen::handler',
    type: 'Object',
    properties: [{
        name: 'action',
        type: 'model::listen::handler.action()',
        attributes: ['OPTIONAL'],
        details: 'Event listener callback.'
    }, {
        name: 'args',
        type: [{type: 'Array', of: 'any'}, 'any'],
        attributes: ['OPTIONAL'],
        details: $`Handler callbacks arguments.`
    }, {
        name: 'once',
        type: 'boolean',
        attributes: ['OPTIONAL'],
        details: 'Flag indicating that handler should trigger at most once.'
    }, {
        name: 'options',
        type: ['EventTarget.addEventListener()::options', 'boolean'],
        attributes: ['OPTIONAL'],
        details: [
            $`Event listener options.`,
            text`A ${'boolean'} behaves like
                ${'EventTarget.addEventListener()::options'}
                ${'capture'} property.`
        ]
    }, {
        name: 'target',
        type: ['model', 'EventTarget', 'model::listen::target()'],
        attributes: ['OPTIONAL'],
        details: $`Event target to which to attach the events listener.`
    }, {
        name: 'accept',
        type: ['model::listen::handler.accept()'],
        attributes: ['OPTIONAL'],
        details: 'Event acknowledgment resolver.'
    }, {
        name: 'prevent',
        type: ['hexadecimal', 'model::listen::handler.prevent()'],
        attributes: ['OPTIONAL'],
        details: [
            `Hexadecimal-encoded bitfields of operation or
                a callback returning such value:`,
            text`${'=:0x1'} ‒ ${'Event.preventDefault()'}`,
            text`${'=:0x2'} ‒ ${'Event.stopPropagation()'}`,
            text`${'=:0x4'} ‒ ${'Event.stopImmediatePropagation()'}`
        ]
    }],
    details: $`${'#:Model events|#model/events'} handler object.`
}, {
    kind: 'callback',
    name: 'model::listen::handler.action()',
    context: {
        type: 'model::context'
    },
    params: [{
        name: '...args',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'Callback arguments, varying length.'
    }, {
        name: 'event',
        type: 'Event',
        attributes: ['REQUIRED'],
        details: 'DOM event object.'
    }, {
        name: 'context',
        type: ['model::context', 'undefined'],
        attributes: ['REQUIRED'],
        details: $`Event target context, ${'undefined'} for unmanaged targets.`
    }],
    details: $`DOM event listener callback. It MAY act also as a handler object
        when defines the ${'OPTIONAL'} properties of ${'model::listen::handler'}
        object.`
}, {
    kind: 'callback',
    name: 'model::listen::handler.accept()',
    context: {
        type: 'model::context'
    },
    params: [{
        name: '...args',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'Callback arguments, varying length.'
    }, {
        name: 'event',
        type: 'Event',
        attributes: ['REQUIRED'],
        details: 'DOM event object.'
    }, {
        name: 'context',
        type: ['model::context', 'undefined'],
        attributes: ['REQUIRED'],
        details: $`Event target context, ${'undefined'} for unmanaged targets.`
    }],
    returns: {
        type: 'boolean'
    },
    details: `DOM event acknowledgment resolver.`
}, {
    kind: 'callback',
    name: 'model::listen::handler.prevent()',
    context: {
        type: 'model::context'
    },
    params: [{
        name: '...args',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'Callback arguments, varying length.'
    }, {
        name: 'event',
        type: 'Event',
        attributes: ['REQUIRED'],
        details: 'DOM event object.'
    }, {
        name: 'context',
        type: ['model::context', 'undefined'],
        attributes: ['REQUIRED'],
        details: $`Event target context, ${'undefined'} for unmanaged targets.`
    }],
    returns: {
        type: 'hexadecimal',
        details: $`A value as specified by ${'model::listen::handler'}
            ${'prevent'} property.`
    },
    details: $`DOM event normalization resolver.`
}, {
    kind: 'callback',
    name: 'model::listen::target()',
    context: {
        type: 'model::context'
    },
    returns: {
        type: ['model', 'EventTarget']
    },
    details: `DOM event target resolver.`
}, {
    kind: 'typedef',
    name: 'model::keymap',
    type: 'Object',
    properties: [{
        name: 'keys',
        type: ['string', {type: 'Array', of: 'string'}],
        attributes: ['REQUIRED'],
        details: [
            $`Keyboard shortcut ${'!:key'} or space delimited
                ${'!:modifiers'} + ${'!:key'}.`,
            text`Built-in ${`#:modifier keys|${MDNAPI}/UI_Events/Keyboard_event_key_values#modifier_keys`}:`,
            text`${'=:Alt'}, ${'=:Option'}
                ‒ ${'#:Alt|//en.wikipedia.org/wiki/Alt_key'} modifier key.`,
            text`${'=:Control'}, ${'=:Ctrl'}
                ‒ ${'#:Control|//en.wikipedia.org/wiki/Control_key'} modifier key.`,
            text`${'=:Meta'}, ${'=:Super'}, ${'=:Command'}
                ‒ ${'#:Meta|//en.wikipedia.org/wiki/Meta_key'} modifier key.`,
            text`${'=:Shift'}
                ‒ ${'#:Shift|//en.wikipedia.org/wiki/Shift_key'} modifier key.`,
            text`Built-in key aliases, an extension of ${`#:standard keys|${MDNAPI}/UI_Events/Keyboard_event_key_values`}:`,
            text`${'=:Left'}, ${'=:Right'}, ${'=:Down'}, ${'=:Up'}
                ‒ ${'#:Arrow|//en.wikipedia.org/wiki/Arrow_keys'} keys.`,
            text`${'=:Menu'} ‒ ${'#:Menu|//en.wikipedia.org/wiki/Menu_key'} key.`,
            text`${'=:Space'} ‒ ${'#:Space|//en.wikipedia.org/wiki/Space_bar'} key.`,
            text`${'=:Return'} ‒ ${'#:Enter|//en.wikipedia.org/wiki/Enter_key'} key.`,
            text`${'=:Esc'} ‒ ${'#:Escape|//en.wikipedia.org/wiki/Esc_key'} key.`,
            $`The ${'=:F1'}‒${'=:F24'} ${'!: '}${`#:function keys|${MDNAPI}/UI_Events/Keyboard_event_key_values#function_keys`}
                MAY be used without modifiers.
                This rule also applies to the following special keys:${'/'}`,
            $`
            ${'=:Enter'} ${'=:Escape'} ${'=:Tab'} ${'=:Backspace'}
            ${'=:Insert'} ${'=:Delete'}${'/'}
            ${'=:CapsLock'} ${'=:NumLock'} ${'=:ScrollLock'} ${'=:ContextMenu'}
            ${'=:Play'} ${'=:Pause'}${'/'}
            ${'=:ArrowLeft'} ${'=:ArrowRight'} ${'=:ArrowDown'} ${'=:ArrowUp'}
            ${'=:Home'} ${'=:End'} ${'=:PageDown'} ${'=:PageUp'}
            `,
        ]
    }, {
        name: 'loose',
        type: 'boolean',
        default: false,
        attributes: ['OPTIONAL'],
        details: $`Loose sequence pressing of ${'!:modifiers'} + ${'!:key'}.${'/'}
            By default, ${'!:modifiers'} are expected to be pressed
            strictly before ${'!:key'}.`
    }, {
        name: 'repeat',
        type: 'boolean',
        default: false,
        attributes: ['OPTIONAL'],
        details: $`Allow ${`#:key repeat|${MDNAPI}/KeyboardEvent/repeat`}
            events for automatic execution of keyboard shortcut handler.${'/'}
            MAY not work properly in environments whith quirky
            ${`#:auto-repeat handling|${MDNAPI}/KeyboardEvent#auto-repeat_handling`}.`
    }, {
        name: 'prevent',
        type: ['hexadecimal', 'model::listen::handler.prevent()'],
        attributes: ['OPTIONAL'],
        default: '0x4',
        details: $`Event operations bitfields,
            see ${'model::listen::handler'} ${'prevent'} for details.`
    }],
    details: $`${'#:Keyboard shortcuts|#extras/keyboard-shortcuts'}
        handler object. Here are defined the properties specific to
        this object only, for an extended handler configuration the
        ${'model::listen::handler'} object should be consulted.`
}, {
    kind: 'typedef',
    name: 'model::animate',
    type: 'Object',
    properties: [{
        name: 'keyframes',
        type: 'Keyframe',
        attributes: ['REQUIRED'],
        details: 'Keyframes options.'
    }, {
        name: 'on',
        type: 'model::animate.on',
        attributes: ['OPTIONAL'],
        details: 'Animation events handler.'
    }, {
        name: 'options',
        type: 'Element.animate()::options',
        attributes: ['OPTIONAL'],
        details: 'Animation options.'
    }, {
        name: 'target',
        type: ['model', 'Element', 'model::animate::target()'],
        attributes: ['OPTIONAL'],
        details: 'Animated target.'
    }],
    details: 'Animations directive definition.'
}, {
    kind: 'typedef',
    name: 'model::animate.on',
    type: 'Object',
    properties: [{
        name: 'ready',
        type: 'model::animate::action()',
        attributes: ['OPTIONAL'],
        details: $`Handles ${'Animation.ready'} event.`
    }, {
        name: 'cancel',
        type: 'model::animate::action()',
        attributes: ['OPTIONAL'],
        details: $`Handles animation
            ${`${'#:cancel'}|${MDNAPI}/Animation/cancel_event`} event.`
    }, {
        name: 'finish',
        type: 'model::animate::action()',
        attributes: ['OPTIONAL'],
        details: $`Handles animation
            ${`${'#:finish'}|${MDNAPI}/Animation/finish_event`} event.`
    }, {
        name: 'remove',
        type: 'model::animate::action()',
        attributes: ['OPTIONAL'],
        details: $`Handles animation
            ${`${'#:remove'}|${MDNAPI}/Animation/remove_event`} event.`
    }],
    details: 'Animation events handler object.'
}, {
    kind: 'callback',
    name: 'model::animate::action()',
    context: {
        type: 'model::context'
    },
    params: [{
        name: 'event',
        type: 'AnimationPlaybackEvent',
        attributes: ['REQUIRED'],
        details: 'Animation playback event.'
    }, {
        name: 'context',
        type: ['model::context', 'undefined'],
        attributes: ['REQUIRED'],
        details: $`Animation target context, ${'undefined'}
            for unmanaged targets.`
    }],
    details: 'Animation events listener callback.'
}, {
    kind: 'callback',
    name: 'model::animate::target()',
    context: {
        type: 'model::context'
    },
    returns: {
        type: ['model', 'Element']
    },
    details: 'Animation target resolver.'
}, {
    kind: 'callback',
    name: 'model.init()',
    context: {
        type: 'model::context'
    },
    details: $`Initialization routine, invoked once the model mount operation
        starts, before the creation of target and any directive tasks.`
}, {
    kind: 'typedef',
    name: 'model.options',
    type: 'Object',
    properties: [{
        name: 'is',
        type: 'string',
        attributes: ['OPTIONAL'],
        details: $`The tag name of a previously defined custom element
            (see ${'#:?:is|//developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/is'}
            global attribute).`
    }, {
        name: 'shadow',
        type: 'model.options.shadow',
        attributes: ['OPTIONAL'],
        details: $`${'ShadowRoot'} options.`
    }, {
        name: 'ttl',
        type: 'integer',
        attributes: ['OPTIONAL'],
        details: $`Grace period (in milliseconds) during which the
            ${'env.vacuum()'} operations will ignore the model,
            a value in range ${`!:0`} ${'!:..'}
            ${`#:MAX_SAFE_INTEGER|${MDNRef}/Number/MAX_SAFE_INTEGER`}.`
    }],
    details: `Model options`
}, {
    kind: 'typedef',
    name: 'model.options.shadow',
    type: 'Object',
    properties: [{
        name: 'css',
        type: [
            {type: 'Array', of: ['CSSStyleSheet']},
            'state()', 'model::resolver()'
        ],
        attributes: ['OPTIONAL'],
        details: $`An array of constructed stylesheets to be set as value of
            ${'ShadowRoot.adoptedStyleSheets'} property or a computable which
            returns a such array.`
    }],
    details: $`The initialization options of the model target ${'ShadowRoot'}.
        ${'/'}
        This definition describes only the system enhanced properties of the
        ${'shadow'} object. For a complete list of properties the
        ${'Element.attachShadow()'} options specification should be consulted.`
}, {
    kind: 'function',
    name: 'Context()',
    imports: `import { Context } from 'karyon';`,
    params: [{
        name: 'ref',
        type: ['model', 'Node'],
        attributes: ['REQUIRED'],
        details: 'Entity reference.'
    }],
    returns: {
        type: ['model::context', 'undefined']
    },
    details: $`Retrieves the context of a certain entity.`
}, {
    kind: 'function',
    name: 'Component()',
    imports: `import { Component } from 'karyon';`,
    params: [{
        name: 'name',
        type: 'string',
        attributes: ['REQUIRED'],
        details: 'Component name.'
    }, {
        name: 'options',
        type: 'Object',
        attributes: ['OPTIONAL'],
        details: $`Options for ${'Component.define()::builder()'} callback.`
    }],
    properties: [{
        name: 'define',
        type: 'Component.define()',
        attributes: ['READONLY'],
        details: 'Defines a new component.'
    }, {
        name: 'owns',
        type: 'Component.owns()',
        attributes: ['READONLY'],
        details: `Validates a component entity.`
    }],
    returns: {
        type: 'model'
    },
    details: 'Creates a new component.'
}, {
    kind: 'method',
    name: 'Component.define()',
    params: [{
        name: 'name',
        type: 'string',
        attributes: ['REQUIRED'],
        details: 'Component name.'
    }, {
        name: 'builder',
        type: 'Component.define()::builder()',
        attributes: ['REQUIRED'],
        details: `Component builder.`
    }, {
        name: 'options',
        type: 'Component.define()::options',
        attributes: ['OPTIONAL'],
        details: `Component initialization options.`
    }],
    throws: {
        type: ['TypeError', 'any'],
        details: [
            text`Component already defined or invalid builder, or`,
            text`${'CustomElementRegistry.define()'} exceptions.`
        ]
    },
    details: 'Defines a new component.'
}, {
    kind: 'callback',
    name: 'Component.define()::builder()',
    context: {
        type: 'any',
        details: $`Value bound by the ${'Component()'} function.`
    },
    params: [{
        name: 'model',
        type: 'model',
        attributes: ['REQUIRED'],
        details: 'Component name.'
    }, {
        name: 'options',
        type: 'any',
        attributes: ['REQUIRED'],
        details: $`Options parameter bound by ${'Component()'} function.`
    }],
    details: `Builder implements the component composition logic.`
}, {
    kind: 'typedef',
    name: 'Component.define()::options',
    type: 'Object',
    properties: [{
        name: 'class',
        type: 'class',
        attributes: ['OPTIONAL'],
        details: $`The ${'#:custom element class|//developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_custom_elements#implementing_a_custom_element'}.`
    }],
    details: $`Custom element definition options.${'/'}
        This definition describes only the system enhanced properties of the
        ${'options'} object. For a complete list of properties the
        ${'#:CustomElementRegistry::define()|//developer.mozilla.org/en-US/docs/Web/API/CustomElementRegistry/define#options'}
        method specification should be consulted.`
}, {
    kind: 'method',
    name: 'Component.owns()',
    params: [{
        name: 'ref',
        type: 'model',
        attributes: ['REQUIRED'],
        details: 'Component entity.'
    }],
    returns: {
        type: 'boolean'
    },
    details: `Validates a component entity.`
}, {
    kind: 'function',
    name: 'State()',
    imports: `import { State } from 'karyon';`,
    params: [{
        name: 'value',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'State initial value.'
    }, {
        name: 'options',
        type: 'State()::options',
        attributes: ['OPTIONAL'],
        details: 'State options.'
    }],
    properties: [{
        name: 'peek',
        type: 'State.peek()',
        attributes: ['READONLY'],
        details: 'Retrieves a state value.'
    }, {
        name: 'flush',
        type: 'State.flush()',
        attributes: ['READONLY'],
        details: 'Evaluates state dependant tasks.'
    }, {
        name: 'batch',
        type: 'State.batch()',
        attributes: ['READONLY'],
        details: 'Batches multiple states update.'
    }, {
        name: 'on',
        type: 'State.on()',
        attributes: ['READONLY'],
        details: 'Registers a state change event handler.'
    }, {
        name: 'off',
        type: 'State.off()',
        attributes: ['READONLY'],
        details: 'Removes a state change event handler.'
    }, {
        name: 'owns',
        type: 'State.owns()',
        attributes: ['READONLY'],
        details: `Validates a state entity.`
    }],
    returns: {
        type: 'state()'
    },
    details: $`Creates a ${'state()'} entity.`
}, {
    kind: 'typedef',
    name: 'State()::options',
    type: 'Object',
    properties: [{
        name: 'accept',
        type: 'State()::options.accept()',
        attributes: ['OPTIONAL'],
        details: 'State value acknowledgment resolver.'
    }, {
        name: 'digest',
        type: 'State()::options.digest()',
        attributes: ['OPTIONAL'],
        details: 'State value normalization resolver.'
    }],
    details: $`Initialization options for ${'state()'} function.`
}, {
    kind: 'callback',
    name: 'State()::options.accept()',
    context: {
        type: 'any'
    },
    params: [{
        name: 'value',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'New value.'
    }, {
        name: 'current',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'Current value.'
    }, {
        name: 'options',
        type: ['state()::options', 'any'],
        attributes: ['REQUIRED'],
        details: $`The ${'.:options'} argument of ${'state()'} function call.`
    }],
    returns: {
        type: 'boolean'
    },
    details: `State value acknowledgment resolver, enforces a conditional
        state update operation.`
}, {
    kind: 'callback',
    name: 'State()::options.digest()',
    context: {
        type: 'any'
    },
    params: [{
        name: 'value',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'New value.'
    }, {
        name: 'current',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'Current value.'
    }, {
        name: 'options',
        type: ['state()::options', 'any'],
        attributes: ['REQUIRED'],
        details: $`The ${'.:options'} argument of ${'state()'} function call.`
    }],
    returns: {
        type: 'any',
        details: 'Normalized value.'
    },
    details: `State value normalization resolver, computes the value for state
        update operation.`
}, {
    kind: 'method',
    name: 'State.peek()',
    params: [{
        name: 'state',
        type: 'state()',
        attributes: ['REQUIRED'],
        details: 'State reference.'
    }],
    returns: {
        type: 'any'
    },
    throws: {
        type: 'ReferenceError',
        details: 'Invalid state reference.'
    },
    details: $`Retrieves a state value while
        bypassing the system dependency tracking.`
}, {
    kind: 'method',
    name: 'State.flush()',
    params: [{
        name: 'state',
        type: 'state()',
        attributes: ['REQUIRED'],
        details: 'State reference.'
    }],
    throws: {
        type: 'ReferenceError',
        details: 'Invalid state reference.'
    },
    details: $`Starts the evaluation cycle for all dependant tasks of a
        certain state.`
}, {
    kind: 'method',
    name: 'State.batch()',
    params: [{
        name: 'resolver',
        type: 'State.batch()::resolver()',
        attributes: ['REQUIRED'],
        details: 'The batch resolver.'
    }, {
        name: '...args',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: $`The arguments for batch resolver.`
    }],
    throws: {
        type: 'any',
        details: $`Unhandled exceptions within ${'State()::options.accept()'} or
            ${'State()::options.digest()'} callbacks of states being updated or
            any batch resolver exception.`
    },
    details: [
        $`Batches multiple ${'state()'} update operations, evaluating in one
            cycle all ${'model::resolver()'} tasks dependant on those states,
            which otherwise would trigger the tasks evaluation for every state
            update.${'/'}
            Subsequent calls of ${'State.batch()'} method inside of
            batch resolver creates batch subtransactions.${'/'}
            It MAY not be suitable when tasks relies on immediate state value,
            as it MAY break the update flow.${'/'}
            State event handlers, i.e. instantiated with ${'State.on()'}
            method, are not subjects of batch operations.`
    ]
}, {
    kind: 'callback',
    name: 'State.batch()::resolver()',
    context: {
        type: 'any',
        details: $`The value of ${'this'} bound by ${'State.batch()'} call.`
    },
    params: [{
        name: '...args',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: $`The batch resolver arguments
            bound by ${'State.batch()'} call.`
    }],
    details: $`A function which implements the states update logic.`
}, {
    kind: 'method',
    name: 'State.on()',
    params: [{
        name: 'state',
        type: 'state()',
        attributes: ['REQUIRED'],
        details: 'State reference.'
    }, {
        name: 'handler',
        type: ['emitter.on()::handler', 'emitter.on()::handler.action()'],
        attributes: ['REQUIRED'],
        details: 'Event handler.'
    }],
    throws: {
        type: ['ReferenceError', 'TypeError'],
        details: 'Invalid state reference or event handler.'
    },
    details: [
        text`Registers a state change event handler. The handler callback
        receives the ${'!:new'} and ${'!:old'} state values,
        the value of ${'this'} is bound by ${'call()'} method of ${'state()'}
        or ${'State.on()'} functions.`,
        code(`
        let id = State(1);
        State.on(id, function (newId, oldId) {
            console.log(this, newId, oldId); // 3 2 1
        });
        id.call(3, 2);
        `)
    ]
}, {
    kind: 'method',
    name: 'State.off()',
    params: [{
        name: 'state',
        type: 'state()',
        attributes: ['REQUIRED'],
        details: 'State reference.'
    }, {
        name: 'handler',
        type: ['emitter.on()::handler', 'emitter.on()::handler.action()'],
        attributes: ['REQUIRED'],
        details: 'Event handler reference.'
    }],
    throws: {
        type: 'ReferenceError',
        details: 'Invalid state reference.'
    },
    details: [
        text`Removes explicitly a state change event handler registered with
            ${'State.on()'} method.`
    ]
}, {
    kind: 'method',
    name: 'State.owns()',
    params: [{
        name: 'state',
        type: 'state()',
        attributes: ['REQUIRED'],
        details: 'State reference.'
    }],
    returns: {
        type: 'boolean'
    },
    details: `Validates a state entity.`
}, {
    kind: 'function',
    name: 'state()',
    params: [{
        name: 'value',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'State new value.'
    }, {
        name: 'options',
        type: ['state()::options', 'any'],
        attributes: ['OPTIONAL'],
        details: 'State value update options.'
    }],
    returns: {
        type: 'any',
        details: 'State value.'
    },
    throws: {
        type: 'any',
        details: $`Unhandled exceptions within ${'State()::options.accept()'} or
            ${'State()::options.digest()'} callbacks.`
    },
    details: $`The reactive entity used to observe and update a dynamic value.`
}, {
    kind: 'typedef',
    name: 'state()::options',
    type: 'Object',
    properties: [{
        name: 'recursive',
        type: 'integer',
        default: 0,
        attributes: ['OPTIONAL'],
        details: $`Recursions limit, a value in range ${`!:0`} ${'!:..'}
            ${`#:MAX_SAFE_INTEGER|${MDNRef}/Number/MAX_SAFE_INTEGER`}.`
    }],
    details: $`Options for ${'state()'} value update.`
}, {
    kind: 'function',
    name: 'insert()',
    imports: `import { insert } from 'karyon';`,
    params: [{
        name: 'content',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'Content to insert.'
    }, {
        name: 'parent',
        type: ['model', 'Element'],
        attributes: ['REQUIRED'],
        details: 'Parent reference.'
    }, {
        name: 'before',
        type: ['model', 'Node'],
        attributes: ['OPTIONAL'],
        details: 'Before reference.'
    }],
    returns: {
        type: 'boolean'
    },
    throws: {
        type: ['TypeError', 'any'],
        details: $`Invalid parent or DOM
            ${'#:pre-insert validity|//dom.spec.whatwg.org/#concept-node-ensure-pre-insertion-validity'}
            exceptions.`
    },
    details: 'Inserts content into DOM.'
}, {
    kind: 'function',
    name: 'remove()',
    imports: `import { remove } from 'karyon';`,
    params: [{
        name: 'ref',
        type: ['model', 'Node'],
        attributes: ['REQUIRED'],
        details: 'Target reference.'
    }],
    details: 'Removes the target reference from DOM.'
}, {
    kind: 'function',
    name: 'evolve()',
    imports: `import { evolve } from 'karyon';`,
    params: [{
        name: 'model',
        type: 'model',
        attributes: ['REQUIRED'],
        details: 'Model reference.'
    }, {
        name: 'mixin',
        type: ['model::mixin', 'model'],
        attributes: ['REQUIRED'],
        details: `Mixin object or reference, or model reference.`
    }, {
        name: 'options',
        type: 'evolve()::options',
        attributes: ['OPTIONAL'],
        details: 'Mixin evolve options.'
    }],
    details: $`Assigns a mixin for a certain model and applies it if model is
        currently mounted, otherwise deferred for a future mount.`
}, {
    kind: 'typedef',
    name: 'evolve()::options',
    type: 'Object',
    properties: [{
        name: 'once',
        type: 'boolean',
        attributes: ['OPTIONAL'],
        details: `Flag indicating that system should apply mixin at most once.`
    }],
    details: 'Mixin evolve options.'
}, {
    kind: 'function',
    name: 'revoke()',
    imports: `import { revoke } from 'karyon';`,
    params: [{
        name: 'model',
        type: 'model',
        attributes: ['REQUIRED'],
        details: 'Model reference.'
    }, {
        name: 'mixin',
        type: ['model::mixin', 'model'],
        attributes: ['REQUIRED'],
        details: `Mixin or model reference.`
    }, {
        name: 'options',
        type: 'revoke()::options',
        attributes: ['OPTIONAL'],
        details: 'Mixin revoke options.'
    }],
    details: $`Revokes a model mixin which was previously assigned by the
        ${'evolve()'} function.`
}, {
    kind: 'typedef',
    name: 'revoke()::options',
    type: 'Object',
    properties: [{
        name: 'remount',
        type: 'boolean',
        attributes: ['OPTIONAL'],
        details: $`Triggers a model remount, similarly to an imperative
            ${'remove()'}/${'insert()'} operation.${'/'}
            Used for purging the mixins artifacts, by reverting the live
            ${'model'} to its original state, the current ${'model::context'}
            after this operation is lost.`
    }],
    details: 'Mixin revoke options.'
}, {
    kind: 'function',
    name: 'message()',
    imports: `import { message } from 'karyon';`,
    params: [{
        name: 'ref',
        type: ['model', 'Node'],
        attributes: ['REQUIRED'],
        details: 'Recipient reference.'
    }, {
        name: '...payload',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'Message payload parameters, zero or more.'
    }],
    details: 'Send messages to recipient.'
}, {
    kind: 'function',
    name: 'multicast()',
    imports: `import { multicast } from 'karyon';`,
    params: [{
        name: 'channel',
        type: ['any'],
        attributes: ['OPTIONAL'],
        details: 'Multicast channel identifier.'
    }, {
        name: '...payload',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'Message payload parameters, zero or more.'
    }],
    details: 'Sends messages to subscribed recipients.'
}, {
    kind: 'function',
    name: 'Emitter()',
    imports: `import { Emitter } from 'karyon';`,
    params: [{
        name: 'options',
        type: 'Emitter()::options',
        attributes: ['OPTIONAL'],
        details: $`${'emitter'} options.`
    }],
    returns: {
        type: 'emitter'
    },
    details: $`Creates and returns a new ${'emitter'} entity.`
}, {
    kind: 'typedef',
    name: 'Emitter()::options',
    type: 'Object',
    properties: [{
        name: 'accept',
        type: 'Emitter()::options.accept()',
        attributes: ['OPTIONAL'],
        details: 'Event acknowledgment resolver.'
    }],
    details: $`${'emitter'} initialization options.`
}, {
    kind: 'callback',
    name: 'Emitter()::options.accept()',
    context: {
        type: 'any'
    },
    params: [{
        type: 'any',
        name: 'event',
        attributes: ['REQUIRED'],
        details: 'Event identifier.'
    }],
    returns: {
        type: 'boolean'
    },
    details: `Acknowledges the handler registration for a certain event.`
}, {
    kind: 'object',
    name: 'emitter',
    type: 'Object',
    properties: [{
        name: 'emit',
        type: 'emitter.emit()',
        attributes: ['READONLY'],
        details: 'Dispatches an event.'
    }, {
        name: 'on',
        type: 'emitter.on()',
        attributes: ['READONLY'],
        details: 'Registers an event handler.'
    }, {
        name: 'off',
        type: 'emitter.off()',
        attributes: ['READONLY'],
        details: 'Removes an event handler.'
    }, {
        name: 'remove',
        type: 'emitter.remove()',
        attributes: ['READONLY'],
        details: 'Removes all event handlers.'
    }, {
        name: 'count',
        type: 'emitter.count()',
        attributes: ['READONLY'],
        details: 'Counts emitter event handlers.'
    }],
    details: `Events emitter object.`
}, {
    kind: 'method',
    name: 'emitter.emit()',
    params: [{
        name: 'event',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'Event identifier.'
    }, {
        name: '...payload',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'Event payload parameters, zero or more.'
    }],
    details: `Dispatches events to registered handlers, synchronously invoking
        each handler callback in order defined by handler priority.`
}, {
    kind: 'method',
    name: 'emitter.on()',
    params: [{
        name: 'event',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'Event identifier.'
    }, {
        name: 'handler',
        type: ['emitter.on()::handler', 'emitter.on()::handler.action()'],
        attributes: ['REQUIRED'],
        details: 'Event handler, unique for a certain event.'
    }],
    returns: {
        type: 'boolean'
    },
    throws: {
        type: ['TypeError', 'any'],
        details: [
            $`Throws in case of:`,
            text`‒ Invalid event handler.`,
            text`‒ Invalid ${'emitter.on()::handler'} ${'priority'} value.`,
            text`‒ Unhandled exceptions within ${'Emitter()::options.accept()'}
                callback.`
        ]
    },
    details: $`Registers an event handler.`
}, {
    kind: 'typedef',
    name: 'emitter.on()::handler',
    type: 'Object',
    properties: [{
        name: 'action',
        type: 'emitter.on()::handler.action()',
        attributes: ['REQUIRED'],
        details: 'Event handler callback.'
    }, {
        name: 'args',
        type: [{type: 'Array', of: 'any'}, 'any'],
        attributes: ['OPTIONAL'],
        details: $`Handler callbacks arguments.`
    }, {
        name: 'once',
        type: 'boolean',
        attributes: ['OPTIONAL'],
        details: `Flag indicating that event handler callback should be invoked
            at most once. When this occurs the handler is removed then the
            callback is invoked afterwards.`
    }, {
        name: 'priority',
        type: 'integer',
        default: 0,
        attributes: ['OPTIONAL'],
        details: $`Event handler priority, defines the execution order of
            handler callback regardless of registration order.${'/'}
            A value in range ${`!:0`} ${'!:..'}
            ${`#:MAX_SAFE_INTEGER|${MDNRef}/Number/MAX_SAFE_INTEGER`}
            (lowest .. highest).`
    }, {
        name: 'recursive',
        type: 'integer',
        default: 0,
        attributes: ['OPTIONAL'],
        details: $`Recursions limit, a value in range ${`!:0`} ${'!:..'}
            ${`#:MAX_SAFE_INTEGER|${MDNRef}/Number/MAX_SAFE_INTEGER`}.`
    }],
    details: $`Object handler for ${'emitter'} events.`,
}, {
    kind: 'callback',
    name: 'emitter.on()::handler.action()',
    context: {
        type: 'any',
        details: $`Value bound by the ${'call()'} method of
            ${'emitter.emit()'} or ${'emitter.on()'}, or handler reference.`
    },
    params: [{
        name: '...args',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: $`The ${'args'} property values of ${'emitter.on()::handler'}
            object.`
    }, {
        name: '...payload',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: $`Payload arguments, as bound by ${'emitter.emit()'} method.`
    }],
    details: $`
        Event handler callback, called synchronously when an event is emitted.
        It MAY act also as a handler object when defines the ${'OPTIONAL'}
        properties of ${'emitter.on()::handler'} object.`
}, {
    kind: 'method',
    name: 'emitter.off()',
    params: [{
        name: 'event',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'Event identifier.'
    }, {
        name: 'handler',
        type: ['emitter.on()::handler', 'emitter.on()::handler.action()'],
        attributes: ['REQUIRED'],
        details: 'Event handler reference.'
    }],
    returns: {
        type: 'boolean'
    },
    details: 'Removes an event handler.'
}, {
    kind: 'method',
    name: 'emitter.remove()',
    params: [{
        name: 'event',
        type: 'any',
        attributes: ['REQUIRED'],
        details: 'Event identifier.'
    }],
    returns: {
        type: 'boolean'
    },
    details: 'Removes all handlers for an event.'
}, {
    kind: 'method',
    name: 'emitter.count()',
    params: [{
        name: 'event',
        type: 'any',
        attributes: ['OPTIONAL'],
        details: 'Event identifier.',
    }],
    returns: {
        type: 'integer'
    },
    details: `Counts emitter event handlers.`
}, {
    kind: 'object',
    name: 'env',
    imports: `import env from 'karyon';`,
    type: 'Object',
    properties: [{
        name: 'VERSION',
        type: 'string',
        attributes: ['READONLY'],
        details: text`Module ${'#:semantic version|//semver.org'}.`
    }, {
        name: 'on',
        type: 'emitter.on()',
        attributes: ['READONLY'],
        details: 'Registers a global event handler.'
    }, {
        name: 'off',
        type: 'emitter.off()',
        attributes: ['READONLY'],
        details: 'Removes a global event handler.'
    }, {
        name: 'vacuum',
        type: 'env.vacuum()',
        attributes: ['READONLY'],
        details: 'Releases unused model mounts.'
    }, {
        name: 'noop',
        type: 'env.noop()',
        attributes: ['READONLY'],
        details: $`A ${'#:no-op|//en.wikipedia.org/wiki/NOP_(code)'} function
            that performs no operations.${'/'}`
    }],
    details: `System utilities.`
}, {
    kind: 'method',
    name: 'env.vacuum()',
    params: [{
        name: 'options',
        type: 'env.vacuum()::options',
        attributes: ['OPTIONAL'],
        details: 'Auxiliary options.'
    }],
    returns: {
        type: {type: 'Array', of: 'model'},
        details: 'An array of models affected by the operation.'
    },
    details: $`Releases inactive model mounts. This method unmounts models when
        their targets are not connected to the live DOM, also disposes the
        underlying tasks of those models mounts. It does not remove the targets
        from the container they are attached to.`
}, {
    kind: 'typedef',
    name: 'env.vacuum()::options',
    type: 'Object',
    properties: [{
        name: 'age',
        type: 'integer',
        default: 60000,
        attributes: ['OPTIONAL'],
        details: $`The minimum age of mounts (in milliseconds since the last
            checkpoint) required to perform the vacuum operation,
            a value in range ${`!:0`} ${'!:..'}
            ${`#:MAX_SAFE_INTEGER|${MDNRef}/Number/MAX_SAFE_INTEGER`}.
            The ${'model.options'} ${'.:ttl'} property value, if defined, take
            precedence over this.`
    }, {
        name: 'analyze',
        type: 'boolean',
        attributes: ['OPTIONAL'],
        details: 'Flag for non-destructive (check-only) operation.'
    }],
    details: $`Options definition for ${'env.vacuum()'} method.`
}, {
    kind: 'method',
    name: 'env.noop()',
    details: $`A ${'#:no-op|//en.wikipedia.org/wiki/NOP_(code)'} function which
        performs no operations. Acts as a sentinel value when a ${'state()'}
        or ${'model::resolver()'} of a certain directive returns this function,
        enforcing the ${'#:idling|#techniques/idling'}
        of underlying DOM operation.`
}];

API.forEach(api => types.set(api.name, `#API/${api.name}`));

const contents = [
    heading({is: 'a', attrs: {href: `#${ID}`}, content: TITLE}),
    {is: 'ul', class: 'section', content: API.map(({kind, name, imports}) => (
        {is: 'li', class: {imports}, content: {
            is: 'a', attrs: {href: `#${ID}/${name}`, 'data-kind': kind},
            content: name
        }}
    ))}
];
const sections = [
    {is: 'h1', id: ID, content: [plink(`#${ID}`), TITLE]},
    {is: 'article', content: [
    text`This section contains ${'KARYON'} API specification reference.`,
    text`The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
        "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and
        "OPTIONAL" in this document are to be interpreted as described in
        ${'#:BCP 14|//tools.ietf.org/html/bcp14'}
        ${'#:[RFC2119]|//tools.ietf.org/html/rfc2119'}
        ${'#:[RFC8174]|//tools.ietf.org/html/rfc8174'}
        when, and only when, they appear in all capitals, as shown here.`,
    text`The word ${'DEFAULT'} defines the preexisting value for an
        ${'OPTIONAL'} field.`,
    text`The word ${'READONLY'} mean that a particular field cannot be assigned,
        either because it is an immutable value or an immutable reference.`,
    text`The word ${'REQUIRED'} in a ${'@:callback'} definition mean that a
        particular argument is always present in the ${'arguments'} of callback
        function.`,
    text`Labels ${'@:typedef'} and ${'@:callback'} denotes user-defined
        entities, all other labels ‒ system entities.`,
    text`The scope resolution operator
        ${'#:?:.:::|//en.wikipedia.org/wiki/Scope_resolution_operator'}
        denotes a correlation between API entities.`,
    ]},
    API.map(api => {
        const {
            kind, name, imports, type, context, params, properties,
            returns, throws, details
        } = api;
        const callable = callables.has(kind);
        return {is: 'article', content: [
            {is: 'h2', id: `${ID}/${name}`, content: [
                plink(`#${ID}/${name}`),
                $`${`@:${kind}`}`,
                {is: 'code', content: name}
            ]},
            type && def('Type', [{type}]),
            callable && context && def('Context', [context]),
            def(kind === 'callback' ? 'Arguments' : 'Parameters', params),
            def('Properties', properties),
            callable && def('Return', [returns ?? {type: 'undefined'}]),
            callable && throws && def('Throw', [throws]),
            imports && [heading('Import'), code(imports)],
            details && [heading('Details'), {class: 'details', content: details}]
        ]};
    })
];

try { (await navigator.serviceWorker.register('/sw.js')).update(); } catch {}

export default { contents, sections };
