import { State, Context, Component, message } from '/karyon.js';

Component.define('page-nav', (model, cfg) => {

    const css = `
    :host {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    :host > div {
        display: flex;
        align-items: center;
        height: 100%;
    }
    @keyframes logo-icon {
        0% { transform: translateX(-150%); }
        33% { transform: translateX(.5em); }
        66% { transform: translateX(-1em); }
    }
    @keyframes logo-name {
        10% { transform: translateX(-150%); }
        33% { transform: translateX(2em); }
        66% { transform: translateX(-.5em); }
    }
    #logo {
        margin-left: 0.5em;
    }
    #logo-icon {
        height: inherit;
        aspect-ratio: 1;
        background: center/calc(100% / 1.5) no-repeat;
        background-image: url(/img/logo-icon.svg);
        animation: logo-icon 1s;
        z-index: 1;
    }
    #logo-name {
        height: inherit;
        aspect-ratio: 1320/560;
        max-width: 30vw;
        background: center/contain no-repeat;
        background-image: url(/img/logo-name.svg);
        animation: logo-name 1s;
    }
    .icon {
        display: flex;
        height: var(--page-nav-size);
        justify-content: center;
        align-items: center;
        padding: 0 1em;
        color: var(--fg);
        text-decoration: none;
        background-color: var(--bg-alt);
    }
    .icon::before {
        content: '';
        height: 1.5em;
        aspect-ratio: 1;
        background: center/contain no-repeat;
    }
    .icon:hover,
    .icon:focus,
    .icon:active {
        background-color: var(--bg-action-alt);
    }
    .icon.handbook::before {
        background-image: url(/img/book.svg);
    }
    .icon.workbench::before {
        background-image: url(/img/object.svg);
    }
    .icon.about::before {
        background-image: url(/img/about.svg);
    }
    .icon.menu::before {
        background-image: url(/img/more.svg);
        transition: background-image .2s;
    }
    .icon.menu[data-active]::before {
        background-image: url(/img/x.svg);
    }
    #actions {
        display: flex;
        bottom: 0;
        background-color: var(--bg-alt);
        touch-action: none;
    }
    .icon::after {
        content: attr(title);
        margin-left: .5em;
        color: #0B3;
        font-size: 1.125em;
        text-transform: capitalize;
        white-space: nowrap;
    }
    .icon:hover::after,
    .icon:focus::after,
    .icon:active::after,
    .icon[data-active]::after {
        color: #FFF;
    }
    .icon.menu {
        display: none;
        position: fixed;
        top: 0;
        aspect-ratio: 1;
        align-self: flex-end;
        z-index: 1;
    }
    .icon.menu::after {
        content: none;
    }
    @media (max-width: 860px) {
        :host > .icons {
            position: fixed;
            z-index: -1;
            top: 0;
            right: 0;
            height: var(--page-nav-size);
        }
        #actions {
            z-index: -1;
            width: 100vw;
            position: absolute;
            right: 0;
            flex-direction: column;
            transition: bottom .1s;
        }
        #actions::after {
            content: '';
            position: fixed;
            top: 0;
            width: 100%;
            height: var(--page-nav-size);
            background-color: var(--bg-alt);
        }
        #actions[data-active] {
            bottom: calc(-300% + -1px);
            border-top: 1px solid var(--bg);
        }
        #actions:not([data-active]) .icon:not(.menu) {
            visibility: hidden;
        }
        .icon {
            padding: 0;
            justify-content: flex-end;
        }
        .icon:not(.menu) {
            transition: visibility .2s;
        }
        .icon::after {
            order: -1;
            margin: 0 1em;
        }
        .icon::before {
            margin-right: calc(var(--page-nav-size)/4);
        }
        .icon.menu {
            display: flex;
        }
    }
    `;
    
    const menu = State();
    const on = () => menu();

    const actions = {
        id: 'actions',
        attrs: {'data-active': on},
        content: [
            {is: 'a', class: 'icon menu',
                attrs: {'data-active': on, href: '#', title: 'Menu'},
                listen: {click: {action () {
                    menu(!menu());
                    menu() && message(cfg.contents, 'off');
                }, prevent: 1}}
            },
            ['handbook', 'workbench', 'about'].map(id => ({
                is: 'a', class: `icon ${id}`,
                attrs: {href: `/${id}/`, title: id,
                    'data-active': id === cfg?.id}
            }))
        ]
    };

    const logo = {id: 'logo', content: cfg?.id && [
        {is: 'a', id: 'logo-icon', attrs: {href: '#', title: 'Contents'},
            listen: {click: {action () {
                message(cfg.contents, 'toggle');
            }, prevent: 1}}
        },
        {is: 'a', id: 'logo-name', attrs: {href: '/', title: 'Home'}}
    ]};

    const shadow = [
        logo,
        {class: 'icons', content: actions,
            listen: {pointerdown: [
                {prevent: 4},
                {action () { menu(null); }, target: window}
            ]}
        },
        {is: 'style', content: css}
    ];

    Object.assign(model, {shadow});
});

Component.define('page-contents', (model, cfg) => {

    const on = State(window.innerWidth >= 360 * 4);
    const off = () => on(null);
    const toggle = () => on(!on());
    const ready = () => setTimeout(() => Context(model)?.target.focus(), 100);
    const messages = {off, toggle};

    Object.assign(model, {
        id: 'contents',
        attrs: {tabindex: -1, 'data-active': () => on()},
        content: [
            {class: 'menu', content: [
                {}, {class: 'action close', listen: {click: off}}
            ]},
            cfg.content
        ],
        keymap: [
            {keys: 'F1', action: toggle, prevent: 5, target: window},
            {keys: 'Escape', action: off}
        ],
        observe: {
            attach () {
                this.target.querySelector(`[href='${location.hash}']`)?.
                    scrollIntoView(true);
                ready();
            },
            message (type) { messages[type]?.(); }
        }
    });
    
    State.on(on, ready);
});
