import { Component, insert } from '/karyon.js';
import { text, section } from './api.js';
import './components.js';

const book = new Set;

book.add(section('License', [
    {content: [
        text`${'KARYON'} module source code is licensed under
            ${'#:MIT License|//gitlab.com/karyonjs/karyon/-/blob/main/LICENSE'}.`,
        text`Documentation and logotypes are licensed under
            ${'#:CC-BY-4.0 License|//creativecommons.org/licenses/by/4.0/'}.`,
    ]},
]));

const sections = {id: 'sections', content: [...book]
    .map(section => ({is: 'section', content: section}))};

const contents = Component('page-contents', {
    content: {is: 'ul', content: sections.content.map(section => {
        const [{id, content: [, title]}, ...articles] = section.content;
        return [
            {is: 'h3', content: {
                is: 'a', attrs: {href: `#${id}`}, content: title}},
            {is: 'ul', content: articles.slice(1).map((article, i) => {
                const [{id, content: [, title]}] = article.content;
                return {is: 'li', content:
                    {is: 'a', attrs: {href: `#${id}`}, content: title}};
            })}
        ];
    })}
});

insert([
    Component('page-nav', {id: 'about', contents}),
    {is: 'main', id: 'about', content: [contents, sections]}
], document.body);
