import { Component, State, insert } from '/karyon.js';
import API, { heading, text, code, list, section } from './api.js';
import './components.js';

let book = new Set;

book.add(section('Overview', [
    {content: [
        text`
        ${'KARYON'} is a
        ${'#:JavaScript module|//developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules'}
        for creating highly composable, reactivity-driven, high-performance
        web application interfaces. It provides convenient and efficient means
        of abstraction from the imperative
        ${'#:DOM API|//developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model'},
        facilitating the development at various levels of complexity.
        `
    ]},
    {title: 'Quickstart', content: [
        text`
        ${'#:Interactive simulator|/workbench'} is a quick way to get a basic
        familiarity with ${'KARYON'}, discover its features also discern its
        limitations.
        It provides a bunch of samples for experiments, such as the ubiquitous
        ${'#:Hello World|/workbench/#basics/hello-world'}
        or esoteric
        ${'#:Sierpiński Triangle|/workbench/#examples/sierpinski-triangle'}.
        `,
        text`An import right from a CDN is suffice to start using the module:`,
        code(`import * as api from '//cdn.jsdelivr.net/npm/karyon';`),
        text`
        Alternatively, the ${'karyon'} package can be installed from
        ${'#:npm|//npmjs.com/package/karyon'}:
        `,
        code(`npm i karyon`, {lang: 'sh'}),
        text`then imported into the module files:`,
        code(`import * as api from 'karyon';`),
        text`
        The import above, as well as in the hereinafter examples, assumes that
        dependencies are handled by a module bundler, such as
        ${'#:esbuild|//esbuild.github.io'}.
        `,
    ]},
    {title: 'Compatibility', content: [
        text`
        ${'KARYON'} is fully functional across major browsers such as
        Chrome, Firefox and Safari. More generally, it should work in any
        browser that supports the
        ${'#:ECMAScript 2022|//262.ecma-international.org/13.0/'}
        specification or higher.
        `
    ]}
]))
.add(section('Model', [
    {content: [
        text`
        The ${'model'} is a declarative interface on top of DOM ${'Node'}.
        It encapsulates everything about how a ${'Node'}, hereinafter referred
        to as ${'!:target'}, should look and behave, involving notions such as
        the
        ${'#:content|#model/content'},
        ${'#:attributes|#model/attributes'},
        ${'#:events|#model/events'},
        ${'#:state|#model/state'},
        ${'#:lifecycle|#model/lifecycle'},
        all defined by special properties, hereinafter referred to as
        ${'!:directives'}.
        `,
        text`
        An empty
        ${'#:object literal|//developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types#object_literals'}
        is the simplest definition of a ${'model'} which materializes into a
        generic ${'<div>'} element. A certain ${'HTML'}, ${'SVG'} or ${'MathML'}
        target can be defined with the ${'::is'} directive, using the respective
        ${'tagName'} or ${'nodeName'} value:
        `,
        code(`
        let div = {};
        let img = { is: 'img' };
        `),
        text`
        The target ${'#:namespace|Element.namespaceURI'} is inferred implicitly,
        however in some cases an explicit namespace is required, which is
        defined through the ${'::ns'} directive. A such case is shown in the
        next example, where a ${'<canvas>'} element of ${'HTML'} namespace is
        embedded into a ${'#:<svg>|SVG.svg'} element of ${'SVG'} namespace:
        `,
        code(`
        let svg = { is: 'svg', content: {
            is: 'foreignObject', content: { is: 'canvas', ns: 'html' }
        }};
        `),
        text`
        Of particular note is that the models are unique entities, just like
        the DOM targets they defines, therefore multiple references of a
        certain model at runtime creates a single target,
        if those references are subject for ${'#:diffing|#techniques/diffing'},
        or triggers errors otherwise.
        `,
        heading('Further Readings'),
        text`${'#:Model reference|#API/model'}`,
        text`${'#:HTML elements reference|//developer.mozilla.org/en-US/docs/Web/HTML/Element'}`,
        text`${'#:SVG elements reference|//developer.mozilla.org/en-US/docs/Web/SVG/Element'}`,
        text`${'#:MathML elements reference|//developer.mozilla.org/en-US/docs/Web/MathML/Element'}`
    ]},
    {title: 'Content', content: [
        text`
        The ${'::content'} directive defines the DOM content of the 
        target upon which its
        ${'#:render tree|//developer.mozilla.org/en-US/docs/Web/Performance/Critical_rendering_path#render_tree'}
        is constructed. The directive can get any value, its handling is
        described hereafter.
        `,
        text`
        The ${'undefined'}, ${'null'} and ${'boolean'} are ${'void'} values
        which does not cary any content.
        The next is an empty ${'<p>'} element definition:
        `,
        code(`let empty = { is: 'p', content: true };`),
        text`
        A ${'primitive'} value, other than ${'void'} values, spawns a
        ${'Text'} node:
        `,
        code(`let pi = { is: 'var', content: 3.14159265 };`),
        text`
        An ${'Object'} value is handled as a ${'model'}, the next is a
        ${'<div>'} containing an ${'<i>'} element:
        `,
        code(`let inset = { content: { is: 'i', content: 'Quod erat demonstrandum.' }};`),
        text`
        An ${'Array'} value defines the target ${'#:child nodes|childNodes'},
        the next is a ${'<q>'} element containing a ${'Text'} node and a
        ${'<div>'} element:
        `,
        code(`let fragment = { is: 'q', content: [ 'To be', { content: 'or not Toby' } ]};`),
        text`
        The ${'Node'} objects have no special handling, an exception being the
        model target nodes which should not be used as content values, since
        that will result to an unexpected system behavior:
        `,
        code(`let text = { content: document.createTextNode('The last thing the author intended...') };`),
        text`
        A ${'function'} value provides a computed result that obeys to the rules
        defined above. When function is a ${'state()'} or a
        ${'model::resolver()'} entity, it serves also as a reactive content.
        The next ${'::content'} directive evaluates to a ${'Text'} node:
        `,
        code(`let compute = { content: () => 'Who Can It Be Now?' };`),
        text`
        Any other types not specified above are handled in the same way as
        ${'void'} values.
        `,
        text`
        The ${'::content'} directive does not implicitly generate
        ${'#:HTML|//developer.mozilla.org/en-US/docs/Glossary/HTML'}
        output from string templates. If that is the goal, such content can be
        handled through ${'innerHTML'} property of ${'::props'} directive.
        `,
        heading('Further Readings'),
        text`${'#:Composition|#composition'}`,
        text`${'#:Reactivity|#reactivity'}`,
        text`${'#:Diffing|#techniques/diffing'}`,
        heading('Quickstart'),
        text`${'#:Model content example|/workbench/#model/content'}`,
    ]},
    {title: 'Properties', content: [
        text`
        The properties of ${'model'} target are handled by ${'::props'}
        directive.
        They are defined as ${'❮key,value❯'} pairs, then during model mount
        they are assigned to the target, except a ${'function'} value which
        provides a computed property, so it is firstly evaluated, then the
        result is set as property. When function is a ${'state()'} or a
        ${'model::resolver()'} entity, it serves also as a reactive property.
        `,
        text`
        The ${'Element.id'} property has, for convenience, an ${'::id'}
        directive,
        which is ignored if not a ${'string'} value, or is overwritten if the
        ${'#:id identifier|Element.id'}
        is defined by the ${'::props'} or ${'::attrs'} directives.
        `,
        text`
        In the example below the ${'HTML'} content of a ${'<div>'} element is
        set through ${'innerHTML'} property:
        `,
        code(`
        let follow = { props: { innerHTML:
            '<a href="//example.com" target="_blank" style="color: red">Example Domain</a>'
        }};
        `),
        text`
        The
        ${'#:onevent properties|//developer.mozilla.org/en-US/docs/Web/Events/Event_handlers#using_onevent_properties'}
        handler callback should be wrapped, otherwise it is invoked during the
        mount of model. If wrapper is a reactive entity specified above, the
        callback will be re-assigned each time reactivity kicks in, thus having
        a reactive event listener. The next example shows an ${'&:onclick'}
        property definition:
        `,
        code(`
        let play = { is: 'button', content: 'Play!',
            props: { onclick () { return () => alert('Ta-da!'); }}
        };
        `),
        heading('Further Readings'),
        text`${'#:DOM element properties|Element.properties'}`,
        heading('Quickstart'),
        text`${'#:Model properties example|/workbench/#model/properties'}`,
    ]},
    {title: 'Attributes', content: [
        text`
        ${'#:Global attributes|HTML.attributes'}
        as well as specific ${'#:element attributes|Element.attributes'}
        are handled by the ${'::attrs'} directive. Aditionally, the ${'::class'}
        and ${'::style'} directives are provided for convenience, which handles
        the homonymous attributes.
        As properties, attributes are defined as ${'❮key,value❯'} pairs,
        however value influences the handling.
        `,
        text`
        The ${'null'}, ${'undefined'} and ${'=:false'} are ${'falsy'} values
        which removes the attribute, a ${'=:true'} value sets it. Any other
        value, except a ${'function'}, is implicitly converted with the
        ${'toString()'} method then its result is set as attribute value.
        In the next example a ${'&:disabled'} attribute is set to a ${'<button>'}
        target:
        `,
        code(`
        let inactive = { is: 'button', attrs: { disabled: true }};
        `),
        text`
        A ${'function'} provides a computed result which obeys to the above
        rules. When it is a ${'state()'} or a ${'model::resolver()'} entity,
        it serves also as a reactive attribute.
        A ${'<button>'} with a reactive ${'&:disabled'} attribute is next:
        `,
        code(`
        import { State } from 'karyon';
        
        let disabled = State(true);
        let toggle = { is: 'button', attrs: { disabled }};
        `),
        text`
        The ${'::class'} and ${'::style'} directives, similarly to ${'::attrs'}
        directive, accepts ${'❮key,value❯'} pairs for definition of
        ${'#:class|Element.class'} and ${'#:style|Element.style'}
        attributes, with values handled according to the above rules.
        However, they have own certain peculiarities described hereafter.
        `,
        text`
        The ${'::class'} directive accepts a ${'truthy'} value to set the class
        name or a ${'falsy'} value to remove it, or a ${'function'} which
        returns such values. It also accepts a space delimited ${'string'}
        as immediate value, which includes the target class names, or a
        ${'function'} which returns an equivalent string. 
        `,
        text`
        The ${'::style'} directive keys can be common
        ${'#:CSS properties or equivalent DOM notations|//developer.mozilla.org/en-US/docs/Web/CSS/Reference#index'},
        or
        ${'#:CSS variables|//developer.mozilla.org/en-US/docs/Web/CSS/--*'}.
        It accepts values similarly to ${'::attrs'} directive, except
        ${'boolean'} values which are handled as ${'falsy'} values. It also
        accepts a ${'string'} as immediate value, which includes the CSS style
        declarations, or a ${'function'} which returns an equivalent string.
        `,
        text`
        The ${'function'} used as immediate value for ${'::class'} and
        ${'::style'} directives can be also a reactive entity described above.
        `,
        text`
        The next example shows the definitions of ${'::class'} and ${'::style'}
        directives using varied keys:
        `,
        code(`
        let styled = { is: 'span', content: ':)',
            class: () => 'flex plain-text',
            style: {
                '--width': '80ch',
                'max-width': 'var(--width)',
                backgroundColor: '#FFF',
                color: () => \`#\${((1 << 24) * Math.random() | 0).toString(16)}\`,
                fontSize: '3em'
            }
        };
        `),
        text`
        The ${'#:data-* attributes|Element.data-*'} as well as
        ${'#:aria-* attributes|Element.aria-*'} are defined as below:
        `,
        code(`
        let home = { is: 'a', content: 'Home',
            attrs: { href: '/',
                'aria-label': 'Home Page',
                'data-type': 'navigator'
            }
        };
        `),
        text`
        ${'#:Event handler content attributes|//www.w3.org/html/wg/spec/webappapis.html#event-handler-content-attributes'}
        are easily defined with plain or computable strings, or reactive
        entities when a reactive event listener is required. The next example
        is an ${'&:onclick'} attribute definition, using a plain string value:
        `,
        code(`
        let test = { is: 'button', content: 'Try it.',
            attrs: { onclick: 'alert("It works!");' }
        };
        `),
        heading('Further Readings'),
        text`${'#:HTML global attributes|HTML.attributes'}`,
        text`${'#:DOM element attributes|Element.attributes'}`,
        heading('Quickstart'),
        text`${'#:Model attributes example|/workbench/#model/attributes'}`,
    ]},
    {title: 'Shadow', content: [
        text`
        The content of ${'ShadowRoot'} is defined by ${'::shadow'} directive and
        accepts the same values as the ${'::content'} directive described in the
        ${'#:Content|#model/content'} section. When that is not a ${'void'}
        value, a shadow DOM is attached to the model target, if the target
        ${'#:supports this feature|//developer.mozilla.org/en-US/docs/Web/API/Element/attachShadow#elements_you_can_attach_a_shadow_to'}.
        `,
        text`A single element defined as shadow content:`,
        code(`let poor = { shadow: { content: 'single <div> in shadow' }};`),
        text`Multiple nodes defined as shadow content:`,
        code(`let rich = { shadow: [ '#text in shadow', { content: '<div> too' }]};`),
        text`
        Additional options for ${'ShadowRoot'}, like its ${'mode'} or
        ${'ShadowRoot.adoptedStyleSheets'}, are configured through the
        ${'model.options.shadow'} object, as shown next:
        `,
        code(`
        let styleSheet = new CSSStyleSheet();
        let closedShadow = {
            shadow: { is: 'span', content: 'Styled <span>' },
            options: { shadow: { mode: 'closed', css: [ styleSheet ] }}
        };
        styleSheet.replace('span { color: orange; }');
        `),
        heading('Further Readings'),
        text`${'#:Composition|#composition'}`,
        text`${'#:ShadowRoot interface|ShadowRoot'}`,
        heading('Quickstart'),
        text`${'#:Model shadow example|/workbench/#model/shadow'}`,
    ]},
    {title: 'Events', content: [
        text`
        ${'#:DOM Events|//developer.mozilla.org/en-US/docs/Web/Events'}
        are handled by ${'::listen'} directive, which provides a flexible way to
        set up event handlers for a certain ${'model'}. Event listeners are
        attached to the model target or to any ${'EventTarget'} defined by the
        event handler. They are removed automatically when the model unmounts,
        or in case they are consumed, that is when they triggers while being
        defined as one-time handlers.
        `,
        text`
        Directive allows attaching the event listeners in a variety of ways.
        Below is a ${'&:click'} event listener definition, using a callback
        handler:
        `,
        code(`
        let control = { is: 'button', content: 'Click me!',
            listen: { click (event) { alert(event.type); }}
        };
        `),
        text`
        With an object handler an exhaustive configuration can be defined for
        a certain listener. The next example shows a delegate listener, having
        as target the global ${'window'} object and using ${'capture'} mode:
        `,
        code(`
        let delegate = { is: 'button', content: 'Click anywhere!',
            listen: { click: {
                target: window,
                options: { capture: true },
                action (event) { alert(\`Clicked \${event.target.tagName}\`); }
            }}
        };
        `),
        text`
        Multiple listeners for a certain event are defined using an ${'Array'},
        which may include mixed handler types, thus, the ${'&:click'} event
        listeners defined above can be composed as follows:
        `,
        code(`
        let info = { is: 'button', content: 'Start',
            listen: { click: [
                event => { alert(event.type); },
                {
                    target: window,
                    options: { capture: true },
                    action (event) { alert(\`Clicked \${event.target.tagName}\`); }
                }
            ]}
        };
        `),
        text`
        Also, directive accepts as immediate value an ${'Array'} which includes
        multiple configurations, as shown next:
        `,
        code(`
        let fun = { is: 'button', content: 'Ready?',
            listen: [
                { click: () => alert('Once!') },
                { click: [ () => alert('Twice!!'), () => alert('Peek-A-Boo!!!') ] }
            ]
        };
        `),
        text`
        The generic
        ${'#:object handler|//developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#the_event_listener_callback'}
        is not supported by the ${'::listen'} directive.
        Also, of particular note is the value of ${'this'} within the handlers
        callback, which is a ${'model::context'} object, not a reference to the
        element as in case when handlers are attached through the generic
        ${'#:addEventListener|//developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#the_value_of_this_within_the_handler'}
        method.
        `,
        text`
        The ${'::listen'} directive does not provide a way to assign reactive
        event handlers, a workaround can be the use of ${'::props'} directive as
        described in the ${'#:Properties|#model/properties'} section.
        Nevertheless, event handlers can be attached dynamically with the help
        of ${'#:mixins|#composition/mixins'}, for this, a ${'model::mixin'}
        containing the ${'::listen'} directive definition is assigned to the
        model through an ${'evolve()'} function call.
        `,
        heading('Further Readings'),
        text`${'#:Events handler reference|#API/model::listen::handler'}`,
        text`${'#:Event listener parameters|//developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#parameters'}`,
        heading('Quickstart'),
        text`${'#:Model events example|/workbench/#model/events'}`,
    ]},
    {title: 'Signals', content: [
        text`
        Signals, handled by ${'::observe'} directive, are ${'model'} built-in
        events of two distinct types:
        `,
        text`${'#:Lifecycle|#model/lifecycle'} signals, dispatched once:`,
        list(
            text`
            ${'&:mount'} ‒ model directives are applied to a target.
            `,
            text`
            ${'&:unmount'} ‒ model tasks defined by directives are
            terminated, target is freed.
            `,
            text`
            ${'&:attach'} ‒ target connects to live DOM, the
            browser render kicks in.
            `,
            text`
            ${'&:detach'} ‒ target disconnects from live DOM, the
            unmount task starts thereafter.
            `,
        ),
        text`
        Runtime signals, dispatched on occasion, when triggered by specific
        events:
        `,
        list(
            text`
            ${'&:error'} ‒ model callbacks exceptions.
            `,
            text`${'&:message'} ‒ triggered by ${'message()'} function.`,
            text`${'&:multicast'} ‒ triggered by ${'multicast()'} function.`,
            text`${'&:mutate'} ‒ ${'MutationObserver'} events.`,
            text`${'&:resize'} ‒ ${'ResizeObserver'} events.`,
            text`${'&:intersect'} ‒ ${'IntersectionObserver'} events.`
        ),
        text`
        The ${'&:mutate'}, ${'&:resize'}, ${'&:intersect'} also ${'&:multicast'}
        are subscription-driven signals, a peculiarity of these is that they
        cannot be prioritized (see ${'emitter.on()::handler'} ${'priority'}).
        `,
        text`
        The ${'::observe'} directive allows observing signals in a variety of
        ways, just like the ${'::listen'} directive. The next is a definition
        for an ${'&:attach'} signal observer using a callback handler:
        `,
        code(`
        let logger = { observe: { attach (target) {
            console.log(\`\${target.tagName} is ATTACHED.\`);
        }}};
        `),
        text`
        Extended configurations can be defined through an object handler.
        The next is an ${'IntersectionObserver'} signal observer
        definition, using a computed ${'root'} viewport with an intersection
        ${'threshold'} of ${'=:0.1'}:
        `,
        code(`
        import { State } from 'karyon';
        
        let logo = { is: 'img', attrs: { src: State() },
            observe: { intersect: {
                options: { root () { return this.parent; }, threshold: 0.1 },
                async action () {
                    let res = await fetch('//karyon.dev/img/logo.svg');
                    this.attrs.src(URL.createObjectURL(await res.blob()));
                }
            }}
        };
        `),
        text`
        Multiple handlers for a certain signal are defined using an ${'Array'},
        as follows:
        `,
        code(`
        let header = { is: 'header', content: 'Is Your Cat from Mars?',
            observe: { mount: [
                () => console.log('Ad-Blocker detected.'),
                () => alert('Please disable your ad-blocker for more news!')
            ]}
        };
        `),
        text`
        Also, directive accepts as immediate value an ${'Array'} which includes
        multiple configurations, as shown next:
        `,
        code(`
        let article = { is: 'article',
            content: 'Scientists say that 8 in 64 Silicon Valley cats appear to be from Mars.',
            observe: [
                { mount: () => console.log('Reader detected.') },
                { attach: [ () => console.log('Welcome!'), () => alert('Enjoy your reading!') ] },
            ]
        };
        `),
        text`
        Of particular note is the ${'&:error'} signal, as it propagates to the
        model ancestors if not handled at the source. Otherwise, when handled
        but expected by an ancestor, the error it carries can be re-thrown to
        propagate the signal upwards:
        `,
        code(`
        let rethrow = { observe: { error (e, context) {
            console.log(context?.target);
            throw e;  // propagate upwards
        }}};
        `),
        text`
        If not handled by any model, a system error is emitted, which can be
        handled with a global handler:
        `,
        code(`
        import env from 'karyon';
        
        env.on('error', console.error);
        `),
        heading('Further Readings'),
        text`${'#:Signals handler reference|#API/model::observe::handler'}`,
        heading('Quickstart'),
        text`${'#:Model signals example|/workbench/#model/signals'}`,
    ]},
    {title: 'State', content: [
        text`
        State is a feature of ${'model'} handled by ${'::state'} directive,
        which allows to dinamically ${'!:hide'}/${'!:show'} the model target.
        It behaves visually like a ${'=:display: none'} ${'CSS'} property,
        while actually it removes the target from the DOM when model is in
        ${'!:hide'} state, then rebuilds it on ${'!:show'} state. If not
        defined, the model is considered in ${'!:show'} state.
        `,
        text`
        When in hide state, even if target is removed from the DOM,
        the model uses anyway a dummy target as a DOM marker, so the
        model cannot define another target. The
        ${'#:Lifecycle|#model/lifecycle'}
        section has a better picture of this.
        `,
        text`
        Below is an example of model ${'::state'} directive managed by a
        ${'state()'} and another managed by a ${'model::resolver()'}:
        `,
        code(`
        import { State } from 'karyon';
        
        let state = State(true);
        let statefullOne = { state };
        let statefullTwo = { state: () => !state() };
        `),
        text`
        Since it destroys the target, it may cause side effects if something
        relies on target DOM state, not of model.
        Nevertheless, the advantage is that it keeps the DOM tree in sync with
        application state, while a CSS solution may bring unwanted side effects
        due to active model event listeners and reactive properties, or CSS
        quirks when using certain selectors such as
        ${'#:combinators|//developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors#combinators'}
        and some of
        ${'#:pseudo selectors|//developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors#pseudo'}.
        It has also a performance gain over a CSS solution,
        as in hide state the memory held by target DOM tree is reclaimed and
        there are no directive tasks which otherwise would consume resources.
        In case of heavy target tree and/or multiple models visible only
        ocassionaly the performance gain may be significant.
        `,
        heading('Further Readings'),
        text`${'#:Model lifecycle|#model/lifecycle'}`,
        text`${'#:Reactivity|#reactivity'}`,
        heading('Quickstart'),
        text`${'#:Model state example|/workbench/#model/state'}`,
    ]},
    {title: 'Context', content: [
        text`
        The context of a ${'model'} provides access to runtime values such as
        the DOM target, root component, also others defined in the
        API specification of ${'model::context'} object. It is created once the
        model is mounted and destroyed when unmounted, and is assigned as
        ${'this'} value for every model callback.
        `,
        text`
        In the next example, tag name of the target is retrieved through the
        ${'this'} value:
        `,
        code(`
        let whoami = { is: 'button', content: 'Who am I?',
            listen: { click () {
                console.log(this.target.tagName); //-> 'BUTTON'
            }}
        };
        `),
        text`
        Besides its built-in properties, it exposes the runtime contextual data,
        including that in its upper mount tree. In the following example the
        context values are logged in each node of the tree:
        `,
        code(`
        let main = { is: 'main', data: 'A',
            init () {
                console.log(0, this.root, this.data, this.meta); //-> 0 undefined 'A' undefined
                this.root = 'X';
                this.meta = 'B';
            },
            content: { is: 'section',
                init () {
                    console.log(1, this.root, this.data, this.meta); //-> 1 'X' 'A' 'B'
                    this.meta = 'C';
                },
                content: { is: 'article', data: 'D',
                    init () {
                        console.log(2, this.root, this.data, this.meta);  //-> 2 'X' 'D' 'C'
                    }
                }
            }
        };
        `),
        text`When building DOM tree in an
        ${'#:imperative fashion|#operations/insert-&-remove'}, the context
        of upper tree is not available until the model target is attached to
        its parent, thus some context values are accessible only asynchronously,
        in such cases they may be retrieved in a ${'queueMicrotask'} callback,
        for example:
        `,
        code(`
        import { insert } from 'karyon';
        
        let section = { is: 'section', data: 789 };
        let article = { is: 'article', content: 'ABC',
            init () {
                console.log(this.data); //-> undefined
                queueMicrotask(() => {
                    console.log(this.data); //-> 789
                });
            }
        };
        
        insert(section, document.body);
        insert(article, section);        
        `),
        text`
        Using
        ${'#:arrow functions|//developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions'}
        as model callbacks may be an issue, since they does not provide an own
        ${'this'}, in such cases context may eventually be retrieved through
        the ${'Context()'} function, by model reference if available:
        `,
        code(`
        import { Context } from 'karyon';
        
        let now = { content: () => {
            let tag = Context(now).target.tagName;
            console.log(tag); //-> 'DIV'
            return Date.now();
        }};
        `),
        heading('Further Readings'),
        text`${'#:Model context reference|#API/model::context'}`,
        heading('Quickstart'),
        text`${'#:Model context example|/workbench/#model/context'}`,
    ]},
    {title: 'Lifecycle', content: [
        text`The ${'model'} lifecycle consists of several phases:`,
        list(
            text`
            ${'READY'} ‒ not in use, no directives applies, no target defined.
            `,
            text`
            ${'ACTIVE'} ‒ in use, no directives applies, no target defined.
            `,
            text`
            ${'ZOMBIE'} ‒ in use, ${'!:hide'} state, ${'::state'} only
            directive applies, defines a dummy target.
            `,
            text`
            ${'MOUNTED'} ‒ in use, ${'!:show'} state, all directives applies,
            defines a common target.
            `,
            text`
            ${'ATTACHED'} ‒ in use, target connected to live DOM.
            `
        ),
        text`
        Phases transition may trigger lifecycle signals described in the
        ${'#:Signals|#model/signals'} section.
        `,
        text`
        The diagram below is a simplified view of model lifecycle, that depicts
        the above description:
        `,
        {is: 'img', attrs: {src: '/img/lifecycle.svg'}},
        text`
        The ${'insert()'} and ${'remove()'} functions are the main operations
        which provides imperative access to the ${'model'} lifecycle.
        A typical usage of ${'insert()'} function is attaching the application
        to the DOM. An atypical usage would be the injection of models into
        arbitrary DOM trees. The ${'remove()'} function is for detaching
        a certain ${'model'} from the DOM.
        `,
        text`This functionality is shown in the example below:`,
        code(`
        import { insert, remove } from 'karyon';
        
        let [greet, to] = [{ content: 'Hello' }, { content: 'For Those About To Rock' }];
        
        insert([greet, to], document.body);
        
        setTimeout(() => {
            remove(greet);
            insert({ content: 'We Salute You' }, document.body);
        }, 3000);
        `),
        heading('Further Readings'),
        text`${'#:Signals|#model/signals'}`,
        text`${'#:Reactivity|#reactivity'}`,
        text`${'#:Insert function reference|#API/insert()'}`,
        text`${'#:Remove function reference|#API/remove()'}`,
        heading('Quickstart'),
        text`${'#:Model lifecycle example|/workbench/#model/lifecycle'}`,
    ]}
]))
.add(section('Extras', [
    {content: [
        text`
        The ${'model'} extras provides useful UI enhancements for a rich user
        experience.
        `,
    ]},
    {title: 'Keyboard Shortcuts', content: [
        text`
        ${'#:Keyboard shortcuts|//en.wikipedia.org/wiki/Keyboard_shortcut'}
        is a keyboard input control enhancement handled by ${'::keymap'}
        directive. The shortcuts are defined as space-delimited
        strings containing a combination of
        ${'#:modifier keys|//en.wikipedia.org/wiki/Modifier_key'} with another
        key, or a special non-modifier key only (see ${'model::keymap'}).
        By default, handlers are assigned to the model target, but can be
        assigned also to any other DOM ${'EventTarget'} through the
        ${'::keymap'} ${'target'} property.
        `,
        text`
        An example of a ${'<button>'} that handles the ${'=:Return'} key
        shortcut is below:
        `,
        code(`
        let date = { is: 'button', content: 'Show Date',
            keymap: { keys: 'Return', action () { alert(new Date()); }}
        };
        `),
        text`
        In the the next example the ${'=:Ctrl'} + ${'=:F12'} keyboard shortcut
        is assigned to ${'window'} object:
        `,
        code(`
        let globalKeys = { is: '#comment',
            keymap: {
                keys: 'Ctrl F12', target: window,
                action () { alert('Global keys active.'); }
            }
        };
        `),
        text`
        Since not all targets receive keyboard events by default, the
        ${'#:tabindex attribute|Element.tabindex'} may be required to enable
        them, for a ${'<span>'} element for example:
        `,
        code(`
        let info = { is: 'span', content: 'For info press "Control I".',
            keymap: {
                keys: 'Ctrl I',
                action () { alert('Nothing to see here.'); }
            },
            attrs: { tabindex: 0 }
        };
        `),
        heading('Further Readings'),
        text`${'#:Keyboard Shortcuts handler reference|#API/model::keymap'}`,
        text`${'#:Key values for keyboard events|//developer.mozilla.org/en-US/docs/Web/API/UI_Events/Keyboard_event_key_values'}`,
        heading('Quickstart'),
        text`${'#:Keyboard Shortcuts example|/workbench/#extras/keyboard-shortcuts'}`,
    ]},
    {title: 'Controlled Inputs', content: [
        text`
        Controlled inputs provides a simplified handling of interactive controls
        and is implicitly enabled when the target is of type ${'<input>'},
        ${'<select>'} or ${'<textarea>'} while the ${'&:value'} property of
        ${'::props'} directive is a ${'state()'}, hereinafter referred to as
        ${'!:controller'}.
        `,
        text`
        That way the ${'&:value'} property of target is configured for
        ${'!:read'}/${'!:write'} operations, so the controller reads from input
        when there is an user input event or writes to the input when controller
        value updates.
        `,
        text`
        Any ${'#:contenteditable|Element.contenteditable'} element can be
        controlled similarly, when the ${'innerHTML'} or ${'textContent'}
        properties are handled by a controller, as defined above.
        `,
        text`An example of ${'<textarea>'} control handling is below:`,
        code(`
        import { State } from 'karyon';
        
        let value = State('Some inoffensive text... meh');
        let textual = { is: 'textarea', props: { value }};
        `),
        text`The next is an editable ${'<div>'} element definition:`,
        code(`
        import { State } from 'karyon';
        
        let innerHTML = State('Dangerous <html> detected!?');
        let editable = { props: { innerHTML }};
        `),
        text`
        Another capability is
        ${'#:multiple|//developer.mozilla.org/en-US/docs/Web/HTML/Attributes/multiple'}
        mode handling for ${'<select>'} control as well as ${'=:file'} type
        inputs and a similar emulated mode for ${'=:checkbox'} type inputs.
        It is enabled when the controller holds an ${'Array'} value.
        Below is an example of multiple ${'<select>'} handling, the ${'value'}
        controller array will contain the selected ${'<option>'} models:
        `,
        code(`
        import { State } from 'karyon';
        
        let value = State([]);
        let selectMany = { is: 'select', props: { value },
            content: [
                { is: 'option', content: 'Carbonara' },
                { is: 'option', content: 'E una Coca Cola' }
            ]
        };
        `),
        text`
        The next example shows multiple ${'=:checkbox'} type inputs handling,
        the ${'value'} controller array will contain the selected
        ${'<input>'} models:
        `,
        code(`
        import { State } from 'karyon';
        
        let value = State([]);
        let checkOne = { is: 'input', props: { type: 'checkbox', value }};
        let checkTwo = { is: 'input', props: { type: 'checkbox', value }};
        `),
        text`
        An useful feature is the controller-driven input validation,
        a digits-only input is in the next example:
        `,
        code(`
        import { State } from 'karyon';
        
        let value = State(0, { accept: i => /^[0-9]+$/.test(i) });
        let input = { is: 'input', props: { value }};
        `),
        heading('Further Readings'),
        text`${'#:State reference|#API/State()'}`,
        text`The ${'<input>'}, ${'<select>'} and ${'<textarea>'} elements reference`,
        heading('Quickstart'),
        text`${'#:Controlled Inputs example|/workbench/#extras/controlled-inputs/textual'}`,
    ]},
    {title: 'Animations', content: [
        text`
        Animations are handled by ${'::animate'} directive which provides a
        declarative interface for
        ${'#:Web Animations API|//developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API'}.
        Multiple keyframes with different timings, targets and event listeners
        can be set up with ease.
        `,
        text`An example of a simple animation definition is next:`,
        code(`
        let play = { animate: {
            keyframes: { transform: [ 'rotateZ(-180deg)' ]},
            options: { duration: 2000, fill: 'both' },
            on: { ready: event => event.target.reverse() }
        }};
        `),
        heading('Further Readings'),
        text`${'#:Animations handler reference|#API/model::animate'}`,
        text`${'#:Web Animations API reference|//developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API'}`,
        heading('Quickstart'),
        text`${'#:Animations example|/workbench/#extras/animations'}`,
    ]}
]))
.add(section('Composition', [
    {content: [
        text`
        ${'KARYON'} embodies powerful composition capabilities based only on
        generic JavaScript objects and primitives. Its flexible ${'model'}
        supports various forms of composition which simplifies the creation
        of components of varying complexity. Some basic composition examples
        are below.
        `,
        text`
        Concatenation using ${'Object.assign()'} or
        ${'#:spread properties|//developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax#spread_in_object_literals'}:
        `,
        code(`
        let base = { is: 'a' };
        let ref1 = Object.assign({ href: '/app1' }, base);
        let ref2 = { ...base, href: '/app2' };
        `),
        text`Aggregation using ${'Array'} collections:`,
        code(`
        let collection = { is: 'p', content: [
            'Some content', { is: 'span', content: 'More content' }
        ]};
        `),
        text`
        Besides the conventional approaches mentioned above, the system offers
        its own composition techniques described hereafter.
        `,
    ]},
    {title: 'Mixins', content: [
        text`
        A powerful composition feature of ${'model'} are mixins, which augments
        the functionality and behavior of a certain model. Despite their name,
        the ${'model'} mixins does not obey to classic concept of mixins,
        where they are inherited by or assigned to the target object.
        Here they may be regarded as a virtual functionality, as they does not
        mutate the model object and can be applied even on immutable model.
        Also, they adds nothing to the model context, unless the context is not
        explicitly mutated by some of mixins tasks.
        `,
        text`
        The ${'model::mixin'} interface resembles that of ${'model'} and lacks
        only some directives which are specific for model initialization phase.
        A certain model can be used as a mixin, in that case only directives
        that are mixin specific will apply.
        `,
        text`
        Mixins for a certain model can be defined statically through the
        ${'::mixins'} directive, or dynamically with the ${'evolve()'}
        and ${'revoke()'} functions.
        `,
        text`An example of mixins usage is below:`,
        code(`
        import { evolve, revoke } from 'karyon';
        
        let click = { listen: { click () { alert("Processing Request..."); }}};
        let enable = { attrs: { disabled: false }};
        let confirm = { is: 'button',
            attrs: { disabled: true },
            mixins: [{ content: 'Confirm Update' }, click ]
        };
         
        evolve(confirm, enable);
        revoke(confirm, click);
        `),
        text`
        Mixins are unique for a certain model, thus, subsequent
        references to an already applied mixin are ignored.
        `,
        text`
        A peculiarity when using ${'::mixins'} directive as well as ${'evolve()'}
        function is that they does not implement a merging strategy, so mixins
        may clash with the model functionality or of its other mixins. Also,
        the ${'revoke()'} function does not implement a rollback logic, thus it
        does not restore the target DOM state which may have been previously
        mutated by the revoked mixin.
        `,
        heading('Further Readings'),
        text`${'#:Model mixin reference|#API/model::mixin'}`,
        text`${'#:Evolve function reference|#API/evolve()'}`,
        text`${'#:Revoke function reference|#API/revoke()'}`,
        heading('Quickstart'),
        text`${'#:Mixins example|/workbench/#composition/mixins'}`,
    ]},
    {title: 'Components', content: [
        text`
        ${'KARYON'} components are nothing but plain objects based on the
        ${'model'} interface. Except the shadow DOM feature borrowed from
        model, they have little in common with classic
        ${'#:Web Components|//developer.mozilla.org/en-US/docs/Web/Web_Components'}.
        `,
        text`
        A typical component is registered with ${'Component.define()'} method,
        which accepts a custom element name also a builder function that make
        use of composition to define the component behavior, as below:
        `,
        code(`
        import { Component, State, evolve } from 'karyon';
        
        Component.define('data-logger', (model, options) => {
            evolve(model, {
                observe: { mount () {
                    console.log(\`\${options.name} logger ready.\`);
                }}
            });
            return Object.assign(model, { data: State() });
        });
        `),
        text`
        Once defined, the components are created with ${'Component()'} function,
        as below:
        `,
        code(`
        import { Component } from 'karyon';
        
        let logger = Component('data-logger', { name: 'System' });
        let app = { content: logger };
         
        logger.data([]);
        `),
        text`
        A distinct feature of components is that they are identified as roots
        for other models, thus any model can access its ancestor component
        through the ${'root'} property of the ${'model::context'} object.
        `,
        text`
        A lightweight variation of components are factory functions, though not
        a replacement of them. A typical use of these is exemplified below:
        `,
        code(`
        let Button = title => ({ is: 'button', content: title });
        let dialog = { content: [ Button('Accept'), Button('Reject') ]};
        `),
        heading('Further Readings'),
        text`${'#:Component reference|#API/Component()'}`,
        heading('Quickstart'),
        text`${'#:Components example|/workbench/#composition/components'}`,
    ]},
]))
.add(section('Reactivity', [
    {content: [
        text`
        As a paramount feature of any declarative UI, reactivity is an important
        capability of ${'KARYON'}. With a simplistic approach but flexible and
        powerful functionality, it includes everything necessary for building
        highly dynamic user interfaces.
        `,
    ]},
    {title: 'States', content: [
        text`
        The ${'state()'} is a basic reactive entity used to observe a dynamic
        value. When used in a ${'model'} scope, as a computed directive
        that handles a DOM operation, such as ${'::props'} or ${'::content'},
        it registers an observable dependency, so that dependant tasks will
        re-run each time the state value changes.
        `,
        text`
        A state change is triggered by invoking it with an argument different
        from current state value, in which case the update operation starts,
        and if successfull, triggers all dependant directives tasks.`,
        text`
        Below is an example of a dynamic ${'::content'} directive:
        `,
        code(`
        import { State } from 'karyon';
        
        let title = State('');
        let menu = { content: title };
        
        title('Menu');
        `),
        text`
        The state is not an exclusive UI entity, its changes can also be
        tracked trough explicit subscriptions, for imperative operations in any
        application scope, this and more is provided by various ${'State()'}
        methods.`,
        text`
        Below is an example of a reactive imperative DOM operation,
        using a subscription:
        `,
        code(`
        import { State } from 'karyon';
        
        let title = State('');
        
        State.on(title, value => document.title = value);
        title('Welcome');
        `),
        heading('Further Readings'),
        text`${'#:State reference|#API/State()'}`,
        heading('Quickstart'),
        text`${'#:States example|/workbench/#reactivity/states'}`,
    ]},
    {title: 'Resolvers', content: [
        text`
        While ${'#:states|#reactivity/states'} are reacting to a single value
        change only, a ${'model::resolver()'} is a more powerful entity,
        which can react to many different changes.
        `,
        text`
        The reactivity of resolver is established when a certain ${'state()'}
        is evaluated whithin the resolver scope, in that case an observable
        dependency is set, so the resolver will run each time the value of
        that state changes.
        `,
        text`
        All resolvers of a certain ${'model'} gets implicit reactivity when its
        contextual ${'data'} property is a ${'state()'} or a reactive resolver.
        The resolver ${'data'} argument in such cases is an implicitly
        computed value.
        `,
        text`
        Below is an example of a ${'<span>'} with a dynamic ${'::content'}
        directive handled by a resolver, which reacts to ${'enabled'} state
        changes:
        `,
        code(`
        import { State } from 'karyon';
        
        let enabled = State(false);
        let status = { is: 'span',
            content: () => enabled() ? 'ON' : 'OFF'
        };
        
        setInterval(on => on(!on()), 1000, enabled);
        `),
        text`
        The next is an example of a ${'<span>'} with a dynamic ${'::content'}
        directive handled by a resolver, having implicit reactivity due to
        reactive contextual ${'data'} property:
        `,
        code(`
        import { State } from 'karyon';
        
        let status = { is: 'span', data: State(false),
            content: on => on ? 'ON' : 'OFF'
        };
        
        setInterval(on => on(!on()), 1000, status.data);
        `),
        heading('Further Readings'),
        text`${'#:Resolver reference|#API/model::resolver()'}`,
        heading('Quickstart'),
        text`${'#:Resolvers example|/workbench/#reactivity/resolvers'}`,
    ]}
]))
.add(section('Techniques', [
    {content: [
        text`
        Once the complexity of an application extends far beyond a simple
        "to-do list" app, certain specialized techniques become essential.
        Here are revealed the features which can offer insights into
        system behavior and, at times, carry practical significance.
        `,
    ]},
    {title: 'Messaging', content: [
        text`
        The system provides versatile messaging capabilities through the
        ${'message()'} and ${'multicast()'} functions. While both serves for
        dispatching ${'#:signals|#model/signals'}, they have distinct
        use cases, the ${'message()'} serving for direct to recipient messages
        while ${'multicast()'} for messaging in a
        ${'#:publish–subscribe|//en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern'}
        way.
        `,
        text`The next is a basic example of using the ${'message()'} function:`,
        code(`
        import { message } from 'karyon';
        
        let receiver = { observe: {
            message (...payload) { console.log(payload); }
        }};
        
        message(receiver, 'S', 'O', 'S');
        `),
        text`
        In the next example the same payload as above is dispatched with the
        ${'multicast()'} function:
        `,
        code(`
        import { multicast } from 'karyon';
        
        let subscriber = { observe: {
            multicast: {
                action (channel, ...payload) { console.log(channel, payload); },
                channels: ['alarm']
            }
        }};
        
        multicast('alarm', 'S', 'O', 'S');
        `),
        heading('Further Readings'),
        text`${'#:Signals|#model/signals'}`,
        text`${'#:Message function reference|#API/message()'}`,
        text`${'#:Multicast function reference|#API/multicast()'}`,
        heading('Quickstart'),
        text`${'#:Messaging example|/workbench/#techniques/messaging'}`
    ]},
    {title: 'Vacuuming', content: [
        text`
        In a normal operation the system automatically unmounts and releases
        the models after their targets are detached from DOM. Nevertheless,
        there can be situations requiring a manual clean-up, which usually may
        occur when the models ${'#:lifecycle|#model/lifecycle'} is managed by
        imperative operations which, when improperly used, could cause memory
        leaks due to stale mounts. For such cases the system exposes the
        ${'env.vacuum()'} method, serving to reclaim the memory consumed by
        those unused model mounts.
        `,
        text`Below is a simple example of vacuuming at predefined intervals:`,
        code(`
        import env from 'karyon';
        
        setInterval(() => env.vacuum(), 600000);
        `),
        heading('Further Readings'),
        text`${'#:Vacuum function reference|#API/env.vacuum()'}`,
        text`${'#:Garbage collection|GC'}`,
        heading('Quickstart'),
        text`${'#:Vacuuming example|/workbench/#techniques/vacuuming'}`
    ]},
    {title: 'Diffing', content: [
        text`
        Diffing is a system internal performance optimization technique aiming
        to minimize DOM operations of the ${'::content'} and ${'::shadow'}
        reactive directives. Although an implementation detail, it may be of
        interest when more control over content output behavior is desired.
        `,
        text`
        On content change the system seek to retain target
        ${'#:child nodes|childNodes'}
        which maps to a ${'model'} in the new content items, then by case
        removes unused nodes, inserts new or sorts the remaining ones.
        To keep the complexity low and performance high it does that by
        matching only immediate children nodes of target.
        `,
        text`
        Thus, to trigger the diffing logic and get a performance gain
        the content should be built from static ${'model'} references, the next
        is an example of how to achieve this:
        `,
        code(`
        import { State, insert } from 'karyon';
        
        let fruits = { content: State([
            { content: '🍓' }, { content: '🍌' }, { content: '🍇' },
        ])};
        let repack = () => {
            let items = fruits.content();
            items.forEach(item => item.i = Math.random());
            items.sort((m, n) => m.i - n.i);
            // reuse old models
            fruits.content([...items]);
        };
        
        setInterval(repack, 1000);
        insert(fruits, document.body);
        `),
        text`
        The opposite applies, thus to bypass the diffing logic the content
        should be updated each time with dynamic values, this is shown in the
        next modified version of previous example:
        `,
        code(`
        import { State, insert } from 'karyon';
        
        let fruits = { content: State([
            { content: '🍓' }, { content: '🍌' }, { content: '🍇' },
        ])};
        let repack = () => {
            let items = fruits.content();
            items.forEach(item => item.i = Math.random());
            items.sort((m, n) => m.i - n.i);
            // spawn new models
            fruits.content(items.map(item => ({...item})));
        };
        
        setInterval(repack, 1000);
        insert(fruits, document.body);
        `),
        text`
        Of particular note are the side effects for each of these approaches.
        So, the performance oriented approach may cause render side effects,
        such as replaying CSS animations, since the system uses remove-insert
        DOM operations to sort the targets.
        The bypass approach, besides render side effects, would trigger also
        lifecycle ${'#:signals|#model/signals'}, since the new models would
        spawn new targets.
        `,
        heading('Further Readings'),
        text`${'#:Model content|#model/content'}`,
        text`${'#:Sorting algorithm|//en.wikipedia.org/wiki/Sorting_algorithm'}`,
        text`${'#:Hash table|//en.wikipedia.org/wiki/Hash_table'}`,
        heading('Quickstart'),
        text`${'#:Diffing example|/workbench/#techniques/diffing'}`,
    ]},
    {title: 'Idling', content: [
        text`
        Idling technique is useful mostly in a reactive context, as a way to
        cancel some DOM operations triggered by a certain directive, thus being
        a performance optimization utility.
        For this, the ${'env.noop()'} function is exposed by the module, which
        serves as sentinel value when returned by computed directives.
        `,
        text`
        The next is an example of a timer which runs at ${'=:1'} second clock
        cycle, while the DOM operation is triggered only on every ${'=:5'}
        seconds, thus displaying only ${'=:0'}, ${'=:5'}, ${'=:10'}, ${'=:15'}
        and so on:
        `,
        code(`
        import env, { State } from 'karyon';
        
        let timer = { data: State(0), content: ticks => ticks % 5 ? env.noop : ticks };
        
        setInterval(ticks => ticks(ticks() + 1), 1000, timer.data);
        `),
        heading('Further Readings'),
        text`${'#:Reactivity|#reactivity'}`,
        heading('Quickstart'),
        text`${'#:Idling example|/workbench/#techniques/idling'}`,
    ]},
]));;

book = [...book, API.sections]
    .map(section => ({is: 'section', content: section}));

const contents = Component('page-contents', {
    content: {is: 'ul', content: book.map(section => {
        const [{id, content: [, title]}, ...articles] = section.content;
        return id === 'API' ? API.contents : [
            {is: 'h3', content: {
                is: 'a', attrs: {href: `#${id}`}, content: title}},
            {is: 'ul', content: articles.slice(1).map(({content}) => {
                const [{id, content: [, title]}] = content;
                return {is: 'li', content:
                    {is: 'a', attrs: {href: `#${id}`}, content: title}};
            })}
        ];
    })}
});

const sections = {id: 'sections', content: State([book[0]])};

const load = (i = 0) => requestAnimationFrame(() => {
    if (i++ < 2)
        return load(i);
    sections.content(book);
    const id = location.hash.slice(1);
    id && document.getElementById(id)?.scrollIntoView(true);
});

load();

insert([
    Component('page-nav', {id: 'handbook', contents}),
    {is: 'main', id: 'handbook', content: [contents, sections]}
], document.body);
