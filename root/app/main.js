import { Component, insert } from '/karyon.js';
import { $ } from './api.js';
import './components.js';

const feat = [
    ['Creative', `Freedom of creativity by means of flexible declarative model,
        rich functionality and powerful composition capabilities.`],
    ['Reactive', `Instant reactivity engaging only dynamic parts of UI,
        through real-time tracking and efficient granular updates.`],
    ['JavaScript', `Pure JavaScript, free of syntax extensions and interspersed
        markup, for unstrained, unconstrained developer experience.`]
];

const main = {
    is: 'main', content: [
        {id: 'intro', content: [
            {id: 'logo'},
            {id: 'feat', content: feat.map(([title, text]) => (
                {class: 'feat', content: [
                    {class: 'name', content: title},
                    {class: 'text', content: text}
                ]}
            ))}
        ]}
    ]
};

insert([
    Component('page-nav'), main,
    {is: 'footer', content: [
        {is: 'code', id: 'about', content: $`powered by ${'KARYON'}`}
    ]}
], document.body);
