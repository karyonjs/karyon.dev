import { Context, insert } from '/karyon.js';

const title = {is: 'h3', content: 'Context'};
const log = {id: 'log', is: 'var', content () {
    const {owner, time} = this;
    return JSON.stringify({owner, time}, null, 1);
}};
const root = {id: 'root', content: {is: 'pre', content: log},
    init () {
        this.owner = this.id;
        this.time = Date.now();
    }
};
const meta = {id: 'meta', is: 'object',
    content (data) { return `${this.id}: ${data}`; },
    init () {
        console.warn(`owner: ${this.owner}`);
        queueMicrotask(() => console.info(`owner: ${this.owner}`));
    },
    data () { return this.target; }
};

setTimeout(() => console.log(Context(title)?.target.nextSibling.id), 1000);

insert([title, root], document.body);
insert(meta, root);
