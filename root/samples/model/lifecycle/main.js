import { State, insert, remove } from '/karyon.js';

const time = () => performance.now().toFixed(1);
const log = signal => console.log(signal, time());
const label = {is: 'label', content: time,
    observe: {
        mount () { log('mount'); },
        unmount () { log('unmount'); },
        attach () { log('attach'); },
        detach () { log('detach'); }
	}
};
const toggle = {is: 'button', content: data => data[0],
	data: State(['insert', 'remove']),
    listen: {click () {
        this.data()[0] === 'insert'
        	? insert(label, document.body)
        	: remove(label);
        this.data([...this.data().reverse()]);
    }}
};

insert(toggle, document.body);
