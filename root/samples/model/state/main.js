import { State, insert } from '/karyon.js';

const clock = {id: 'clock', state: State(true),
    content: () => new Date().toLocaleTimeString()
};
const toggle = {id: 'toggle', is: 'button',
    content: () => clock.state() ? 'Hide' : 'Show',
    listen: {click () { clock.state(!clock.state()); }}
};

insert([toggle, clock], document.body);
