import { insert } from '/karyon.js';

const observe = {
    mount () { console.log('mount', this.target); },
    unmount () { console.log('unmount', this.target); },
    attach () { console.log('attach', this.target); },
    detach () { console.log('detach', this.target); }
};
const css = [new CSSStyleSheet(), new CSSStyleSheet()];
const one = {is: 'p', content: 'ONE'};
const two = {
    shadow: [
        {is: 'p', content: 'TWO', observe, listen: {
            click () { this.target.remove(); }}
        },
        {is: 'slot', attrs: {name: 'FOUR'}}
    ],
    content: [
        {is: 'p', content: 'THREE', observe},
        {is: 'p', content: 'FOUR', attrs: {slot: 'FOUR'}}
    ],
    options: {shadow: {css}}
};

css[0].replace(`* { font-size: 1.5em; }`);

setInterval(colors => {
    colors.reverse();
	css[1].replace(`* {
    	color: ${colors[0]};
        text-shadow: 0 0 4px ${colors[1]};
     }`);
}, 300, ['black', 'white']);

insert([one, two], document.body);
