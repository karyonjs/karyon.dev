import { State, insert } from '/karyon.js';

const time = State(0);

setInterval(() => time(Date.now()), 321);

insert([
    true,
    Math.PI,
    time,
    () => `time: ${new Date(time()).toLocaleTimeString()}`,
    ['ticks', ': ', {is: '#text', content: () => `${time()}`.slice(-4)}],
    {is: 'button', content: document.createTextNode('OK')}
].map(content => ({content})), document.body);
