import env, { multicast, insert } from '/karyon.js';

const input = {is: 'input', attrs: {placeholder: 'message'},
    observe: {
        attach () { console.log('input: ready'); },
        resize () { console.warn('input: resize'); },
        multicast: {channels: ['button', 'input'], action (channel, info) {
            const message = this.target.value;
            console.info(`${channel}[${info}]: "${message}"`);
            if (!message)
                throw 'empty message';
        }},
        error (e) {
            console.warn(`input: ${e.message}`);
            throw e;
        }
    },
    keymap: {keys: 'Return', action () { multicast('input', 'submit'); }}
};
const submit = {is: 'button', content: 'submit',
    listen: {click () { multicast('button', 'click'); }}
};

env.on('error', e => console.error(`SYSERR: ${e.message}`));

insert([input, submit], document.body);
