import { State, insert } from '/karyon.js';

const trigger = {is: 'button', content: 'Alarm',
    class: {control: true},
    style: {
        '--width': '14ch',
        'min-width': 'var(--width)',
        color: '#DDD',
        borderColor: 'var(--color)'
    },
    attrs: {
        disabled: false,
        onclick: `alert('Achtung!');`,
        'aria-label': 'Alarm'
    }
};
const beacon = {on: State(),
    class: 'beacon', style: {opacity () { return this.on() ? 1 : .3}},
    attrs: {'data-on' () { return this.on(); }}
};
const device = {content: [trigger, beacon],
    class: 'device', style: 'border: 1px solid var(--color);'
};

setInterval(on => on(!on()), 400, beacon.on);

insert(device, document.body);
