import { insert } from '/karyon.js';

const query = {is: 'button', content: 'Query',
    props: {onclick () {
        return () => alert(document.querySelector('input').value);
    }}
};
const value = {props: {innerHTML: '<input placeholder="VALUE">'}};

insert([value, query], document.body);
