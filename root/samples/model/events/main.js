import { State, insert } from '/karyon.js';

const init = {x: 50, y: 50};
const ball = {id: 'ball', data: State(init),
    listen: {
        pointerdown (e) {
            this.target.setPointerCapture(this.pid = e.pointerId);
            const r = this.target.getBoundingClientRect();
            this.x = e.offsetX - r.width / 2;
            this.y = e.offsetY - r.height / 2;
        },
        pointermove (e) {
            if (!this.target.hasPointerCapture(this.pid))
                return;
            const x = (e.clientX - this.x) / window.innerWidth * 100;
            const y = (e.clientY - this.y) / window.innerHeight * 100;
            this.data({x, y});
        },
        pointerup () {
            this.target.releasePointerCapture(this.pid);
            this.data(init);
        }
    },
    style: {transform: c => `translate(${c.x}vw, ${c.y}vh)`}
};

State.on(ball.data, console.info);

insert(ball, document.body);
