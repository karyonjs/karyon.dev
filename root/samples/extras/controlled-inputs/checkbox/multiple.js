import { State } from '/karyon.js';

const value = State([]);
const inputs = [...'ABC'].map(label =>
    ({is: 'input', props: {value, type: 'checkbox'}, label}));

value([inputs[1]]);

export default [
    inputs.map(i => ({is: 'label', content: [i, i.label]})),
    {is: 'p', content: () => `[ ${value().map(i => i.label).join(' ')} ]`}
];
