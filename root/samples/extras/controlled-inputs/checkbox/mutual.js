import { State } from '/karyon.js';

const value = State();
const values = State([]);
const mutual = {is: 'input', props: {value, type: 'checkbox'}};
const inputs = [...'ABC'].map(label =>
    ({is: 'input', props: {value: values, type: 'checkbox'}, label}));

State.on(value, one => values(one ? [...inputs] : []));
State.on(values, all => value(all.length === inputs.length));
value(true);

export default [
    {is: 'label', content: [mutual, 'All']},
    ...inputs.map(i => ({is: 'label', content: [i, i.label]})),
    {is: 'p', content: () => `[ ${values().map(i => i.label).join(' ')} ]`}
];
