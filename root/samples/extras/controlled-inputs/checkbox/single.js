import { State } from '/karyon.js';

const value = State(false);
const input = {is: 'input', props: {value, type: 'checkbox'}};

export default [
    {is: 'label', content: [input, 'On']},
    {is: 'p', content: () => `${value()}`}
];
