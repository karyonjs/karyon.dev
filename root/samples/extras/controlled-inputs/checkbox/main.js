import { insert } from '/karyon.js';
import single from './single.js';
import multiple from './multiple.js';
import radio from './radio.js';
import mutual from './mutual.js';

insert([
    ['Single:', single],
    ['Multiple:', multiple],
    ['Radio:', radio],
    ['Mutual:', mutual]
].map(([content, items]) => [{is: 'span', content}, ...items]), document.body);
