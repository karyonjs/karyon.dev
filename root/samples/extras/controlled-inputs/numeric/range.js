import { State } from '/karyon.js';

const value = State(5);
const input = {is: 'input', props: {value, min: 0, max: 10, type: 'range'}};

export default { input, value };
