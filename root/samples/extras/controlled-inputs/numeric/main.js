import { insert } from '/karyon.js';
import number from './number.js';
import range from './range.js';

const n = number.value;
const r = range.value;

insert([
    ['Number:', number.input],
    ['Range:', range.input],
    [() => `${n()} + ${r()} = ${n() + r()}`]
].map(([content, item]) => [{is: 'label', content}, item]), document.body);
