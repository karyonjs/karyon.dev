import { State } from '/karyon.js';

const value = State(5, {digest: value => Math.min(Math.max(value, 2), 8)});
const input = {is: 'input', props: {value, min: 0, max: 10, type: 'number'}};

export default { input, value };
