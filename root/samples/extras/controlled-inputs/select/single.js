import { State } from '/karyon.js';

const value0 = State();
const value1 = State();
const options0 = [...'ABC'].map(label =>
    ({is: 'option', label, content: `option: ${label}`}));
const options1 = options0.map(option => ({...option}));

value0(options0[0]);
value1(options1[1]);

export default [
    {is: 'select', props: {value: value0}, content: options0},
    {is: 'p', content: () => value0()?.label},
    {is: 'select', props: {value: value1}, content: options1,
        attrs: {size: options1.length + 1}},
    {is: 'p', content: () => value1()?.label}
];
