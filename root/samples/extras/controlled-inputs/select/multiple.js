import { State } from '/karyon.js';

const value = State([]);
const options = [...'ABC'].map(label =>
    ({is: 'option', label, content: `option: ${label}`}));

value([options[2]]);

export default [
    {is: 'select', props: {value}, content: options},
    {is: 'p', content: () => `[ ${value().map(s => s.label).join(' ')} ]`}
];
