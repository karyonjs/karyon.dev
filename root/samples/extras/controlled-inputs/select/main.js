import { insert } from '/karyon.js';
import single from './single.js';
import multiple from './multiple.js';

insert([
    ['Single:', single],
    ['Multiple:', multiple],
].map(([content, item]) => [{is: 'label', content}, item]), document.body);
