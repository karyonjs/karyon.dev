import { State } from '/karyon.js';

const value = State(`
I write, erase, rewrite
Erase again, and then
A poppy blooms.
`);
const input = {is: 'textarea', props: {value}};

export default { input, value };
