import { insert } from '/karyon.js';
import text from './text.js';
import textarea from './textarea.js';

insert([
    ['Text:', text.input],
    ['Textarea:', textarea.input],
    [() => (text.value() + textarea.value()).match(/\w+/g)?.length ?? 0]
].map(([content, item]) => [{is: 'label', content}, item]), document.body);
