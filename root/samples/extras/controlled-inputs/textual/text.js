import { State } from '/karyon.js';

const value = State('A Poppy Blooms');
const input = {is: 'input', props: {value}, attrs: {placeholder: '...'}};

export default { input, value };
