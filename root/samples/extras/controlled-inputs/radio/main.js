import { State, insert } from '/karyon.js';

const value = State();
const inputs = [...'ABC'].map(label =>
    ({is: 'input', props: {value, type: 'radio', name: 'abc'}, label}));
const content = inputs.map(i => ({is: 'label', content: [i, i.label]}));
const result = {is: 'p', content: () => value()?.label};

value(inputs[1]);

insert([{is: 'span', content: 'Radio:'}, content, result], document.body);
