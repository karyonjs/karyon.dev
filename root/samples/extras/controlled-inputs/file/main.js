import { State, insert } from '/karyon.js';
import single from './single.js';
import multiple from './multiple.js';

const result = {
    content () {
        const list = [single.value(), ...multiple.value()]
            .filter(file => file);
        return !!list.length && [
            {is: 'label', content: 'File list:'},
            list.map(file => [
                {is: 'p', content: `${file.name}: ${file.size} bytes`},
                {content: State(), observe: {async mount () {
                    this.content((await file.text()).slice(0, 100));
                }}}
            ])
        ];
    }
};

insert([
    single.label, single.input, multiple.label, multiple.input, result
], document.body);
