import { State } from '/karyon.js';

const value = State();
const input = {
    is: 'input', id: 'image', props: {value, type: 'file'},
    attrs: {accept: 'image/*'}
};
const label = {is: 'label', attrs: {for: 'image'}, content: 'Single:'};

export default { input, label, value };
