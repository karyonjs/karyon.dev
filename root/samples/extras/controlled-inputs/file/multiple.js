import { State } from '/karyon.js';

const value = State([new File([Date()], 'date.log', {type: 'text/plain'})]);
const input = {is: 'input', id: 'files', props: {value, type: 'file'}};
const label = {is: 'label', attrs: {for: 'files'}, content: 'Multiple:'};

export default { input, label, value };
