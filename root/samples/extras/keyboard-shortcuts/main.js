import { State, insert } from '/karyon.js';

const rtl = State(false);

insert({
    is: 'p', content: 'Press "Ctrl Alt R" to reverse this text.',
    keymap: {keys: 'Ctrl Alt R', action () { rtl(!rtl()); }},
    attrs: {tabindex: 0, 'data-rtl': rtl}
}, document.body);
