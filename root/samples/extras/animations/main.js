import { insert } from '/karyon.js';

const play = {content: 'Yeehaw!', animate: {
    keyframes: {transform: ['rotateZ(-180deg)']},
    options: {duration: 3000, fill: 'both'},
    on: {ready: event => event.target.reverse()}
}};

insert(play, document.body);
