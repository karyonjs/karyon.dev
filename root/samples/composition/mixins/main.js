import { State, evolve, revoke, insert } from '/karyon.js';

const translate = State();
const lang = {
    en: {content: 'Hello!',
        listen: {click: () => translate(['How are you?', 'en', 'fr'])}},
    fr: {content: 'Bonjour!',
        listen: {click: () => translate(['Comment ça va?', 'fr', 'en'])}}
};
const greet = {is: 'button', mixins: [{class: 'greet'}]};

State.on(translate, ([text, from, into]) => {
    revoke(greet, lang[from]);
    evolve(greet, lang[into]);
    alert(text);
});
evolve(greet, lang.en);

insert(greet, document.body);
