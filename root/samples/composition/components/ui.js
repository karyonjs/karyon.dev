import { Component, State, evolve } from '/karyon.js';

const css = [
    [...document.styleSheets[0].cssRules].map(rule => rule.cssText).join(''),
	`
    @keyframes show { 0% { opacity: 0; } }
    pre {
        color: #6BF;
        margin: 1em;
        white-space: pre-wrap;
        animation: show .3s;
    }
    button {
        width: 100%;
        margin: .25em 0 0 0;
        font-family: monospace;
        text-transform: uppercase;
    }
    button[data-active] {
        color: #DDD;
        background-color: #2489;
        border-color: #48F;
    }
    `
].map(rules => {
    const css = new CSSStyleSheet();
    return css.replaceSync(rules), css;
});
const options = {shadow: {css}};

Component.define('ui-foldable', (model, cfg) => {
    const active = State();
    const shadow = cfg.map(({title, content}, i) => [
        {is: 'button', content: title,
            attrs: {'data-active': () => active() === i},
            listen: {click: [
                {action () { active(i); }, prevent: 4},
                {action () { active(null); }, target: window}
            ]},
            keymap: {keys: 'Esc', action () { active(null); }}
        },
        {is: 'pre', content, state: () => active() === i}
    ]);
    evolve(model, {shadow, options});
});
