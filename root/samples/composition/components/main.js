import { Component, insert } from '/karyon.js';
import './ui.js';

const text = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed turpis et libero euismod bibendum convallis non sapien. Sed dictum scelerisque rhoncus. Nulla quis faucibus lectus. Donec sed tortor eget quam dapibus ornare non a quam. Nunc ac risus eu dolor eleifend iaculis. Phasellus facilisis, urna ac porttitor efficitur, eros nisi accumsan felis, eget tempus massa tortor vel nisi. Sed pulvinar mi arcu, ac aliquet leo sodales ut. In consequat augue id nisl tincidunt, non pellentesque risus suscipit. Praesent tristique elit nec dolor consectetur pharetra. Quisque id neque ultrices, eleifend enim nec, mattis lorem.`;

const foldable = Component('ui-foldable', [
    {title: 'overview', content: text.match(/([^.]+[.]){1,1}/)},
    {title: 'rationale', content: text.match(/([^.]+[.]){1,3}/)},
    {title: 'research', content: text}
]);

insert(foldable, document.body);
