export default [{
    type: 'basics',
    name: 'Basics',
    samples: [{
        name: 'Hello World',
        path: 'basics/hello-world',
        files: ['main.js', 'main.css']
    }, {
        name: 'Composition',
        path: 'basics/composition',
        files: ['main.js', 'main.css']
    }, {
        name: 'Reactivity',
        path: 'basics/reactivity',
        files: ['main.js', 'main.css']
    }]
}, {
    type: 'model',
    name: 'Model',
    samples: [{
        name: 'Content',
        path: 'model/content',
        files: ['main.js', 'main.css']
    }, {
        name: 'Properties',
        path: 'model/properties',
        files: ['main.js', 'main.css']
    }, {
        name: 'Attributes',
        path: 'model/attributes',
        files: ['main.js', 'main.css']
    }, {
        name: 'Shadow',
        path: 'model/shadow',
        files: ['main.js', 'main.css'],
        logger: {active: true, timing: true}
    }, {
        name: 'Events',
        path: 'model/events',
        files: ['main.js', 'main.css'],
        logger: {active: true}
    }, {
        name: 'Signals',
        path: 'model/signals',
        files: ['main.js', 'main.css'],
        logger: {active: true, timing: true}
    }, {
        name: 'State',
        path: 'model/state',
        files: ['main.js', 'main.css']
    }, {
        name: 'Context',
        path: 'model/context',
        files: ['main.js', 'main.css'],
        logger: {active: true, timing: true}
    }, {
        name: 'Lifecycle',
        path: 'model/lifecycle',
        files: ['main.js', 'main.css'],
        logger: {active: true, timing: true}
    }]
}, {
    type: 'extras',
    name: 'Extras',
    samples: [{
        name: 'Keyboard Shortcuts',
        path: 'extras/keyboard-shortcuts',
        files: ['main.js', 'main.css']
    }, {
        name: 'Controlled Inputs ‒ Textual',
        path: 'extras/controlled-inputs/textual',
        files: ['main.js', 'main.css', 'text.js', 'textarea.js']
    }, {
        name: 'Controlled Inputs ‒ Numeric',
        path: 'extras/controlled-inputs/numeric',
        files: ['main.js', 'main.css', 'number.js', 'range.js']
    }, {
        name: 'Controlled Inputs ‒ Checkbox',
        path: 'extras/controlled-inputs/checkbox',
        files: [
            'main.js', 'main.css',
            'single.js', 'multiple.js', 'radio.js', 'mutual.js'
        ]
    }, {
        name: 'Controlled Inputs ‒ Radio',
        path: 'extras/controlled-inputs/radio',
        files: ['main.js', 'main.css']
    }, {
        name: 'Controlled Inputs ‒ Select',
        path: 'extras/controlled-inputs/select',
        files: ['main.js', 'main.css', 'single.js', 'multiple.js']
    }, {
        name: 'Controlled Inputs ‒ File',
        path: 'extras/controlled-inputs/file',
        files: ['main.js', 'main.css', 'single.js', 'multiple.js']
    }, {
        name: 'Animations',
        path: 'extras/animations',
        files: ['main.js', 'main.css']
    }]
}, {
    type: 'composition',
    name: 'Composition',
    samples: [{
        name: 'Mixins',
        path: 'composition/mixins',
        files: ['main.js', 'main.css']
    }, {
        name: 'Components',
        path: 'composition/components',
        files: ['main.js', 'main.css', 'ui.js']
    }]
}, {
    type: 'reactivity',
    name: 'Reactivity',
    samples: [{
        name: 'States',
        path: 'reactivity/states',
        files: ['main.js', 'main.css'],
        logger: {active: true, timing: true}
    }, {
        name: 'Resolvers',
        path: 'reactivity/resolvers',
        files: ['main.js', 'main.css']
    }]
}, {
    type: 'techniques',
    name: 'Techniques',
    samples: [{
        name: 'Messaging',
        path: 'techniques/messaging',
        files: ['main.js', 'main.css'],
        logger: {active: true, timing: true}
    }, {
        name: 'Vacuuming',
        path: 'techniques/vacuuming',
        files: ['main.js', 'main.css', 'gc.js'],
        logger: {active: true, timing: true}
    }, {
        name: 'Diffing',
        path: 'techniques/diffing',
        files: ['main.js', 'main.css', 'utils.js'],
        logger: {active: true, timing: true}
    }, {
        name: 'Idling',
        path: 'techniques/idling',
        files: ['main.js', 'main.css'],
        logger: {active: true, timing: true}
    }]
}, {
    type: 'examples',
    name: 'Examples',
    samples: [{
        name: 'To-Do List',
        path: 'examples/to-do',
        files: ['main.js', 'main.css']
    }, {
        name: 'X & O Game',
        path: 'examples/x-o',
        files: ['main.js', 'main.css']
    }, {
        name: 'SVG Clock',
        path: 'examples/clock',
        files: ['main.js', 'main.css']
    }, {
        name: 'Color Noise',
        path: 'examples/color-noise',
        files: ['main.js', 'main.css']
    }, {
        name: 'Sierpiński Triangle',
        path: 'examples/sierpinski-triangle',
        files: ['main.js', 'main.css']
    }, {
        name: '7GUIs ‒ Counter',
        path: 'examples/7guis/counter',
        files: ['main.js', 'main.css']
    }, {
        name: '7GUIs ‒ Temperature Converter',
        path: 'examples/7guis/temperature-converter',
        files: ['main.js', 'main.css']
    }, {
        name: '7GUIs ‒ Flight Booker',
        path: 'examples/7guis/flight-booker',
        files: ['main.js', 'main.css']
    }, {
        name: '7GUIs ‒ Timer',
        path: 'examples/7guis/timer',
        files: ['main.js', 'main.css']
    }, {
        name: '7GUIs ‒ CRUD',
        path: 'examples/7guis/crud',
        files: ['main.js', 'main.css']
    }, {
        name: '7GUIs ‒ Circle Drawer',
        path: 'examples/7guis/circle-drawer',
        files: ['main.js', 'main.css']
    }]
}];
