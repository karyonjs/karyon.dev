import env, { State, insert } from '/karyon.js';

const tick = State(0);
const timers = [
    ['fast', 'info', {content: t => t}],
    ['lazy', 'warn', {content: t => t % 5 ? env.noop : t}]
].map(([id, level, model]) => ({
    is: 'p', id, level, content: [id, model], data: tick,
    observe: {mutate: {
        action () { console[this.level](this.id); },
        options: {characterData: true, subtree: true}
    }}
}));

setInterval(i => tick(tick() + i), 300, 1);

insert(timers, document.body);
