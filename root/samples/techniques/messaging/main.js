import { message, multicast, State, insert } from '/karyon.js';

const receiver = {is: 'var', data: State([]),
    content (data) { return `Receiver: ${data.length} messages`; },
    observe: {message (...payload) {
        console.log('message', ...payload);
        this.data([...this.data(), payload]);
    }}
};
const subscriber = {is: 'var', data: State([]),
    content (data) { return `Subscriber: ${data.length} messages`; },
    observe: {multicast: {action (channel, ...payload) {
        console[channel]?.('multicast', ...payload);
        this.data([...this.data(), [channel, payload]]);
    }, channels: ['info', 'warn']}
}};

insert([
    {content: [{is: 'button', content: 'Message',
    	listen: {click: () => message(receiver, 'S', 'O', 'S')}
	}, receiver]},
    {content: [{is: 'button', content: 'Multicast',
    	listen: {click: () => {
            multicast('info', 'O', 'K', '!');
            multicast('warn', 'S', 'O', 'S');
        }}
	}, subscriber]}
], document.body);
