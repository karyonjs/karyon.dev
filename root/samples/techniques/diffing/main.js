import { State, insert } from '/karyon.js';
import * as utl from './utils.js';

const fruits = [...'🍇🍎🍉🍒🍋🍏🍌🍓'].map(fruit => ({
    class: 'fruit', content: fruit,
    observe: {attach () { this.log('+') }, detach () { this.log('-') }},
    log (tag) { console.log(tag, this.content); }
}));
const basket = {content: State(fruits)};
const repack = {is: 'button', content: 'repack', listen: {click () {
    utl.snapshot('old', basket.content(), basket);
    basket.content(utl.repack(fruits));
    queueMicrotask(() => utl.snapshot('new', basket.content(), basket));
}}};
const inspect = {is: 'label', content: [
    {is: 'input', props: {type: 'checkbox', value: utl.inspector(basket)}},
    'inspect'
]};

insert([repack, inspect, basket], document.body);
