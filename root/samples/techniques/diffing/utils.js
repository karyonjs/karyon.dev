import { Context, State, evolve, revoke } from '/karyon.js';

export const repack = array => {
    for (let a = array, i = a.length - 1, j; i > 0; i--)
        j = Math.floor(Math.random() * (i + 1)), [a[i], a[j]] = [a[j], a[i]];
    return array.slice(-Math.floor(array.length * Math.random()));
};

export const snapshot = (name, source, target) => {
    const {textContent: content} = Context(target).target;
    const items = source.map(item => item.content).join('');
    console[content === items ? 'log' : 'error'](name, items);
};

export const inspector = (mixin => model => {
    const state = State();
    State.on(state, on => (on ? evolve : revoke)(model, mixin));
    return state;
})({observe: {mutate: {options: {childList: true}, action (records) {
    console.info(records.map(({addedNodes, removedNodes}) => ({
        attach: [...addedNodes], detach: [...removedNodes]
    })));
}}}});
