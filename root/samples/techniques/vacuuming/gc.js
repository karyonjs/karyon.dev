import env from '/karyon.js';

const analyze = (age = 100) => ({
    total: env.mounts,
    stale: env.vacuum({analyze: true, age})
});

export const vacuum = (age = 100) => {
    const stats = analyze(age);
    console.info('ANALYZE', stats);
    stats.stale.length && console.warn('VACUUM', env.vacuum({age}));
};

addEventListener('DOMContentLoaded', () => analyze());
