import { insert } from '/karyon.js';
import * as gc from './gc.js';

const create = items => [...items].map(content => ({content}));
const vacuum = {is: 'button', content: 'VACUUM',
	listen: {click: () => gc.vacuum()}
};

insert(vacuum, document.body);
insert(create('🥎📌'), document.body);
insert(create('🧩💊'), document.createDocumentFragment());
