// https://claudiopro.github.io/react-fiber-vs-stack-demo/fiber.html
import { State, insert } from '/karyon.js';

let raf;
const [SIZE, size, scaleDelay, ticksDelay] = [State(400), 25, 500, 100];
const [active, time] = [State(), State(0)];
const loop = now => (time(now), raf = requestAnimationFrame(loop));
const ticks = () => (time() / ticksDelay | 0) % 9 + 1;
const scale = () => {
    const e = time() / scaleDelay % 10;
    return (1 + (e > 5 ? 10 - e : e) / 10) / 2.1;
};
const resize = {is: 'input', props: {type: 'range', value: SIZE, max: 1000}};
const Vertex = ({x, y, s, text}) => ({
    class: 'vertex', style: {'--s': s, '--x': x, '--y': y},
    content: {is: '#text', content: text},
    attrs: {'data-active' () { return this === active(); }},
    listen: {pointerenter () { active(this); }}
});
const Triangle = ({x, y, s, text}) => s > size ? [
    {x, y: y - (s /= 2) / 2, s, text},
    {x: x - s, y: y + s / 2, s, text},
    {x: x + s, y: y + s / 2, s, text}
].map(Triangle) : Vertex({x: x - size / 2, y: y - size / 2, s: size, text});
const Fractal = () => ({
    class: 'fractal', style: {transform: () =>`scale(${scale()}, 0.7)`},
    content: () => Triangle({x: 0, y: 0, s: SIZE(), text: ticks}),
    observe: {mount () { loop(0); }, unmount () { cancelAnimationFrame(raf); }}
});

insert([resize, Fractal], document.body);
