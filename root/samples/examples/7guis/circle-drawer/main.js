// https://7guis.github.io/7guis/tasks/#circle
import { State, insert } from '/karyon.js';

const records = [];
const [id, adjust, circle, radius] = [State(-1), State(), State(), State()];
const r = () => State(Math.max(4, Math.random() * 100));
const circles = () => records[id()]?.circles ?? [];
const active = function () { return circle() === this.model; };
const select = ({model}) => circle(model?.is === 'circle' ? model : null);
const point = () => `(${circle()?.attrs.cx | 0}, ${circle()?.attrs.cy | 0})`;
const record = (sizes, ...items) => {
    records.splice(id() + 1);
    id(records.push({sizes, circles: [...circles(), ...items]}) - 1);
};
const play = {action () {
    adjust(null);
    const i = id(id() + (this.undo ? -1 : 1));
    const {r, r0, r1} = records[this.undo ? i + 1 : i]?.sizes ?? {};
    r?.(this.undo ? r0 : r1);
    !circles().includes(circle()) && circle(null);
}, ended () { return this.undo ? id() < 0 : id() >= records.length - 1; }};
const pointerdown = {action (event, context) {
    adjust(null);
    if (select(context))
        return;
    const {offsetX: cx, offsetY: cy} = event;
    record(null, circle({is: 'circle', attrs: {cx, cy, r: r(), active}}));
}, accept: event => event.button === 0};
const contextmenu = {action (event, context) {
    const r = select(context)?.attrs.r;
    adjust(r ? {r, r0: r()} : null);
}, prevent: 5};

State.on(adjust, (current, previous) => {
    const {r, r0} = previous ?? {}, r1 = r?.();
    r0 !== r1 && record({r, r0, r1}), radius(current?.r());
});
State.on(radius, r => adjust()?.r(r));

insert([
    {id: 'frame', state: adjust, attrs: {'data-point': point}, content:
        {is: 'input', props: {type: 'range', min: 4, value: radius}}},
    [['undo', play, play.ended, true], ['redo', play, play.ended]].map
        (([content, click, disabled, undo]) =>
        ({is: 'button', listen: {click}, attrs: {disabled}, content, undo})),
    {is: 'svg', content: circles, listen: {contextmenu, pointerdown}}
], document.body);
