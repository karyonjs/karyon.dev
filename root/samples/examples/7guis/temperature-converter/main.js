// https://7guis.github.io/7guis/tasks/#temp
import { State, insert } from '/karyon.js';

const [C, F] = [State(0), State(32)];

State.on(C, t => F(+(t * 9 / 5 + 32).toFixed(1)));
State.on(F, t => C(+((t - 32) * 5 / 9).toFixed(1)));

insert([['°C: ', C], ['°F: ', F]].map(([label, value]) => ({content: [
    label, {is: 'input', props: {value, type: 'number', step: .1}}
]})), document.body);
