// https://7guis.github.io/7guis/tasks/#crud
import { State, insert } from '/karyon.js';

const filter = State('');
const select = State();
const text = ['name', 'surname'].map(key => ({key, value: State()}));
const Row = data => ({is: 'option', data, content: data => list() &&
    `${data.name}, ${data.surname}`});
const list = State([
    {name: 'Jean-Luc', surname: 'Picard'},
    {name: 'Benjamin', surname: 'Sisko'},
    {name: 'James', surname: 'Kirk'}
].map(Row));
const view = () => list().filter(function ({data: {surname: text}}) {
    return !this || text.toUpperCase().startsWith(this.toUpperCase());
}, filter());

State.on(filter, () => !view().includes(select()) && select(null));
State.on(select, row => row && text
    .forEach(({key, value}) => value(row.data[key])));

insert([
    {is: 'input', props: {value: filter},
        attrs: {placeholder: 'filter prefix'}},
    {is: 'select', props: {value: select}, attrs: {size: 6}, content: view},
    text.map(({key, value}) => ({is: 'input', props: {value},
        attrs: {placeholder: key}})),
    {class: 'buttons', content: [
        ['create', () => {
            const data = Object.fromEntries(text.map(i => [i.key, i.value()]));
            list([...list(), Row(data)]);
        }, () => text.find(({value}) => !value())],
        ['update', () => {
            const data = select().data;
            text.forEach(({key, value}) => data[key] = value());
            list([...list()]);
        }, () => !select()],
        ['delete', () => {
            const items = new Set(list());
            items.delete(select()) && select(null);
            list([...items]);
        }, () => !select()]].map(([content, click, disabled]) =>
        ({is: 'button', listen: {click}, attrs: {disabled}, content}))}
], document.body);
