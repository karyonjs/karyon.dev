// https://7guis.github.io/7guis/tasks/#timer
import { State, insert } from '/karyon.js';

const [duration, elapsed] = [State(10000), State(0)];
const loop = (fts => now => {
    const min = elapsed(), max = duration();
    max > min && elapsed(Math.min(min + now - fts, max)), fts = now;
    requestAnimationFrame(loop);
})(0);

insert([
    ['elapsed time: ', {is: 'progress', observe: {attach () { loop(0); }},
        props: {value: () => elapsed() / duration()}}],
    () => `${(elapsed() / 1000).toFixed(1)} s`,
    ['duration: ', {is: 'input',
        props: {value: duration, type: 'range', min: 1, max: 60000}}],
    {is: 'button', listen: {click () { elapsed(0); }}, content: 'reset'}
].map(content => ({content})), document.body);
