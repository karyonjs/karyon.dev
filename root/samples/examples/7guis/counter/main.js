// https://7guis.github.io/7guis/tasks/#counter
import { State, insert } from '/karyon.js';

const value = State(0);

insert([
    {is: 'input', props: {value, type: 'number'}},
    {is: 'button', listen: {click: () => value(value() + 1)}, content: 'count'}
], document.body);
