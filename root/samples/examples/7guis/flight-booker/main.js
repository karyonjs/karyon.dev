// https://7guis.github.io/7guis/tasks/#flight
import { State, insert } from '/karyon.js';

const isoDate = (date = new Date()) => {
    const time = date.getTime() - date.getTimezoneOffset() * 60000;
    return new Date(time).toISOString().slice(0, 10);
};
const options = ['One-way flight', 'Return flight'];
const [select, D1, D2] = [State(), State(isoDate()), State(isoDate())];
const isNaD = D => isNaN(D = new Date(D())) || isoDate() > isoDate(D);
const date = fn => new Date(fn()).toLocaleDateString();
const back = () => select()?.content === options[1];
const book = () => alert(`BOOKED: ${select().content},
    depart on ${date(D1)}${back() ? `, return on ${date(D2)}` : ''}.`);
const fail = function () { return isNaD(this.props.value); };

insert([
    {is: 'select', props: {value: select},
        content: options.map(content => ({is: 'option', content})),
        observe: {mount () { select(this.content[0]); }}},
    ...[[D1], [D2, () => !back()]].map(([value, disabled]) =>
        ({is: 'input', props: {type: 'date', value},
            attrs: {disabled, 'data-fail': fail}})),
    {is: 'button', listen: {click: book}, content: 'BOOK', attrs:
        {disabled: () => isNaD(D1) || back() && (isNaD(D2) || D1() > D2())}}
].map(content => ({content})), document.body);
