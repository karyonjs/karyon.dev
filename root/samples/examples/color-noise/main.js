import { State, insert } from '/karyon.js';

let raf;
const size = 60, time = State();
const loop = now => (time(now), raf = requestAnimationFrame(loop));
const halt = () => cancelAnimationFrame(raf);
const color = () => `#${((1 << 24) * Math.random() | 0).toString(16)}`;
const pixels = [...Array(size ** 2)].map(() => ({
    class: 'pixel', style: {'--color': color}, data: time
}));

insert({id: 'canvas', content: pixels, style: {'--size': size},
    observe: {mount () { loop(0); }, unmount () { halt(); }}
}, document.body);
