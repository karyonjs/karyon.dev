import { State, insert } from '/karyon.js';

const Clock = () => {
    const now = () => Date.now() - new Date().getTimezoneOffset() * 60000;
    const time = State();
    const shift = () => time(now());
    const A = T => T / 1000 % 86400;
    const H = T => (A(T) / 3600 * 5 | 0) * 6;
    const M = T => (A(T) % 3600 / 60 | 0) * 6;
    const S = T => (A(T) % 60 | 0) * 6;
    const style = {transform () {
        return `rotate(${this.angle ?? this.fn(time())}deg)`;
    }};
    const ticks = [...Array(60).keys()].map(t => t % 5 ?
        ['line', {y1: 88, y2: 94}, 'tick m', t * 6] :
        ['circle', {cy: 91, r: 3}, 'tick h', t % 12 * 30])
    	.map(([is, attrs, c, a]) => ({is, attrs, style, class: c, angle: a}));
    const content = [
        {is: 'circle', class: 'face', attrs: {r: 98}}, ticks,
        {fn: H, is: 'rect', class: 'hand h', style,
            attrs: {width: 8, height: 80, y: -60, x: -4, rx: 4}},
        {fn: M, is: 'rect', class: 'hand m', style,
            attrs: {width: 8, height: 100, y: -80, x: -4, rx: 4}},
        {fn: S, is: 'path', class: 'hand s', style,
            attrs: {d: 'M0 -84 L2 20 h-4 z'}},
        {is: 'circle', class: 'axis', attrs: {r: 4}}
    ];
    
    return {
        is: 'svg', class: 'clock', content,
        attrs: {viewBox: '-100 -100 200 200'},
        observe: {
            mount () { this.tid = setInterval(shift, 1000), shift(); },
            unmount () { clearInterval(this.tid); }
        }
    };
};

insert(Clock, document.body);
