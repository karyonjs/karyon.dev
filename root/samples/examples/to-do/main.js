import { State, insert } from '/karyon.js';

const key = 'To-Do';
const name = State('');
const items = State([]);
const item = name => ({is: 'li', content: name, listen: {click: remove}});
const load = () => {
    const names = JSON.parse(localStorage.getItem(key)) ?? [];
    items(names.map(item));
};
const save = () => {
    const names = JSON.stringify(items().map(i => i.content));
    localStorage.setItem(key, names);
};
const submit = () => {
    items([item(name()), ...items()]), name(''), save();
};
const remove = function () {
    const list = new Set(items());
    list.delete(this.model), items([...list]), save();
};
const clear = () => (items([]), localStorage.removeItem(key));

insert([
    {is: 'input', props: {value: name, placeholder: key}},
    {is: 'button', content: 'submit', listen: {click: submit},
     	attrs: {disabled: () => !name().trim()}},
    {is: 'button', content: 'clear', listen: {click: clear},
    	attrs: {disabled: () => !items().length}},
    {is: 'ul', content: items, observe: {mount: load}}
], document.body);
