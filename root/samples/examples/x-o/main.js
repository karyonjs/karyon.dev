import { State, multicast, insert } from '/karyon.js';

const wins = [
    [0, 1, 2], [0, 3, 6], [0, 4, 8],
    [3, 4, 5], [1, 4, 7], [2, 4, 6],
    [6, 7, 8], [2, 5, 8]
];
const [win, pick, next] = [State(), State(), State(0)];
const [players, labels] = [['You', 'Bot'], ['X', 'O']];
const player = () => players[next()];
const voids = () => fields.filter(field => !field.fill());
const play = (set => ({fill}) => !win() && !fill() && set(fill))(fill => {
    fill(labels[next()]);
    if (!win(wins.find(win => !win.some(i => fields[i].fill() !== fill()))))
        voids().length ? next(+!next(), {recursive: 1}) : win([]);
});
const again = () => (next(null), win(null), multicast('reset'), next(0));
const solve = (tactics => () => {
    const {content: x} = pick(), cx = fields[4].fill();
    for (const match of tactics[next()])
        for (const win of wins)
            if (match === win.map(i => fields[i].fill()).join(''))
                for (const w of win)
                    if (!fields[w].fill() && (match.length > 1 ||
                        (!cx ? w === 4 : cx === x ? w !== 4 && !(w % 2) :
                        fields[w + Math.sign(w - 4)]?.fill() === x)))
                        return fields[w];
    return (v => v[`${Math.random()}`[2] % v.length])(voids());
})([['XX', 'OO', 'O'], ['OO', 'XX', 'X']]);
const mode = labels.map(content => ({is: 'option', content}));
const info = {id: 'info', state: win, listen: {click () { again(); }},
    attrs: {title: () => win().length ? `${player()} won!` : `It's a tie!`}};
const fields = [...Array(9)].map((fill = State(''), i) => ({
    is: 'button', class: 'field', fill,
    attrs: {title: fill, 'data-win': () => win()?.includes(i)},
    listen: {click () { player() === 'You' && play(this); }},
    observe: {multicast: {channels: 'reset', action () { fill(''); }}}}));

pick(mode[0]);
State.on(pick, () => players.reverse() && again());
State.on(next, () => player() === 'Bot' && play(solve()));

insert([
    {id: 'menu', content: {is: 'select', props: {value: pick}, content: mode}},
    {id: 'game', content: [fields, info], keymap: {keys: 'Esc', action: again}}
], document.body);
