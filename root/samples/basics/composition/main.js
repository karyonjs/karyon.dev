import { insert } from '/karyon.js';

const layout = [
    ['header', 'h3', `System features`],
    ['main', 'pre', `
    A complex system that works is invariably found to have evolved from a simple system that works.
    A complex system designed from scratch never works and cannot be patched up to make it work. You have to start over, beginning with a working simple system.
    `],
    ['footer', 'i', `Build your systems wisely.`]
].map(([type, is, content]) => ({is: type, content: {is, content}}));

insert(layout, document.body);
