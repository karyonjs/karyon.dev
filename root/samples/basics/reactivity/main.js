import { State, insert } from '/karyon.js';

const counter = {is: 'button', value: State(0),
    content () { return `Count: ${this.value()}`; },
    listen: {click () { this.value(this.value() + 1); }}
};

insert(counter, document.body);
