import { State, insert } from '/karyon.js';

let X = 0, Y = State(0);

const resolvers = [
	() => `X = ${X}`,
	() => `Y = ${Y()}`,
	() => `X + Y = ${X + Y()}`
];

setInterval(() => (X++, Y(Y() % 3 + 1)), 300);

insert(resolvers.map((r, i) => (
    {content: [i, ': ', {is: 'var', content: r}]}
)), document.body);
