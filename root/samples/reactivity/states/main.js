import { State, insert } from '/karyon.js';

const X = State(0), Y = State({value: 0});

const update = () => X(X(X(X() + 1) + 1) + 1);
const mutate = () => Y().value++;
const batch = () => State.batch(update);
const flush = () => State.flush(Y);

const on = fn => ({is: 'button', content: fn.name, listen: {click: fn}});

insert([
    {content: [on(update), on(batch)]},
	{content: [on(mutate), on(flush)]},
    {is: 'p', content: () => ({is: '#text', content: X() + Y().value}),
     observe: {mutate: {options: {childList: true},
        action (r) { console.log(...r.flatMap(r => [...r.addedNodes])); }
    }}}
], document.body);
