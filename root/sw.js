const VERSION = '0';

const cacheSelect = event => {
    const req = event.request;
    const url = new URL(req.url);
    if (req.method !== 'GET' || !/^http/.test(url.protocol) ||
        !/karyon\.dev$/.test(url.origin) ||
        !/(\.(html|js|css|svg|png))$/.test(url.pathname))
        return;
    const path = url;
    event.respondWith(caches.match(path).then(res => {
        if (res)
            return res.clone();
        return fetch(path).then(res => {
            if (res.status === 200)
                caches.open('*').then(cache => cache.put(path, res.clone()));
            return res.clone();
        });
    }));
};

const cacheDelete = data => {
    const {src, match} = data ?? {};
    const regex = typeof match === 'string' ? new RegExp(match, 'gi') : null;
    return caches.keys().then(keys => Promise.all(keys.map(key =>
        (!src || src === key) && caches.open(key)
        .then(cache => cache.keys().then(reqs =>
            Promise.all(reqs.map(req => {
                const path = new URL(req.url).pathname;
                return !regex?.test(path) && cache.delete(req);
        })).then(() => cache.keys()
            .then(reqs => !reqs.length && caches.delete(key))
        )))
    )));
};

self.addEventListener('install', () => {
    self.skipWaiting();
});

self.addEventListener('activate', event => {
    event.waitUntil(cacheDelete());
});

self.addEventListener('fetch', cacheSelect);
